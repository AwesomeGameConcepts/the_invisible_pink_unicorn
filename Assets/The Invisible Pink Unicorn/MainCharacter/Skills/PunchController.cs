using AGC;
using Assets.Ekioo.Framework.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Player))]
public class PunchController : MonoBehaviour
{
    [SerializeField]
    private GameObject _bam;

    private ContactFilter2D _contactfilter;

    [SerializeField]
    private FloatData _cooldown;

    [SerializeField]
    private FloatData _damagesAmount;

    private List<RaycastHit2D> _hits;

    [SerializeField]
    private Collider2D _punchCollider;

    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private FloatData _warmUp;

    [SerializeField]
    private BooleanData _isGrounded;

    private Player _player;

    private float _lastHit;

    [SerializeField] private FloatData m_ProjectionForce;

    public async void OnPunch(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0)
            return;

        if (context.phase != InputActionPhase.Performed)
            return;
        if (!_player.InFrontPizzaStand)
        {
            if (Time.time - _lastHit < _cooldown)
                return;

            _lastHit = Time.time;

            _animator.SetTrigger("Punch");

            await Task.Delay((int)(_warmUp.Value * 1000));
            if (TestHit())
            {
                AudioManager.Instance.PlayChangeFightSound();
            }
            else
            {
                AudioManager.Instance.PlayChangeAirFightSound();
            }
        }
        else
        {
            _player.BuyPizza();
        }
    }

    private void Awake()
    {
        _contactfilter.useTriggers = false;
        _contactfilter.useLayerMask = true;
        _contactfilter.SetLayerMask(LayerMaskHelper.Mob);
        _hits = new List<RaycastHit2D>();
        _player = GetComponent<Player>();
    }

    private bool TestHit()
    {
        _hits.Clear();
        if (_punchCollider.Cast(Vector2.zero, _contactfilter, _hits) > 0)
        {
            for (int i = 0; i < _hits.Count; i++)
            {
                if (_hits[i].transform.GetComponent<Enemy>() != null)
                {
                    _hits[i].transform.GetComponent<Enemy>().TakeDamage(_damagesAmount, -_hits[i].normal * m_ProjectionForce, _player);
                    InstantiateFeedback(_hits[i].collider);
                }
            }
            return true;
        }
        else return false;
    }

    private void InstantiateFeedback(Collider2D collider)
    {
        var closestPoint = collider.ClosestPoint(new Vector2(_punchCollider.transform.position.x, _punchCollider.transform.position.y)) + Random.insideUnitCircle * 0.5f;
        Instantiate(_bam, closestPoint, Quaternion.identity);
    }
}