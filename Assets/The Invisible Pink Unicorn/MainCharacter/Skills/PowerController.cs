using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Inventory))]
public class PowerController : MonoBehaviour
{
    private Inventory _inventory;

    [SerializeField]
    private Rigidbody2D _rigidbody2D;

    private Sock _leftSock;
    private Sock _rightSock;

    private void Start()
    {
        _inventory = GetComponent<Inventory>();
    }

    public void OnPowerLeft(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            _leftSock = _inventory.LeftSock;
            StartPower(_leftSock);
        }
        else if (context.phase == InputActionPhase.Canceled)
        {
            EndPower(_leftSock);
        }
    }

    public void OnPowerRight(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            _rightSock = _inventory.RightSock;
            StartPower(_rightSock);
        }
        else if (context.phase == InputActionPhase.Canceled)
        {
            EndPower(_rightSock);
        }
    }

    private void EndPower(Sock sock)
    {
        if (sock == null)
            return;

        switch (sock.Power)
        {

            case Sock.SockPower.Stinky:
                break;

            case Sock.SockPower.Superman:
                break;
        }
    }

    private void StartPower(Sock sock)
    {
        if (sock == null)
            return;

        switch (sock.Power)
        {

            case Sock.SockPower.Stinky:
                break;

            case Sock.SockPower.Superman:
                break;
        }
    }
}