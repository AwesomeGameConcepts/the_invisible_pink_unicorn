using UnityEngine;

public class PushController : MonoBehaviour
{
    private ContactFilter2D _contactfilter;

    private Collider2D[] _hitColliders;

    [SerializeField]
    private Collider2D _pushCollider;

    [SerializeField]
    private Animator _animator;

    private void Awake()
    {
        _contactfilter.useTriggers = true;
        _contactfilter.useLayerMask = true;
        _contactfilter.SetLayerMask(LayerMaskHelper.Platform);

        _hitColliders = new Collider2D[1];
    }

    private bool TestHit()
    {
        int hitCount = _pushCollider.OverlapCollider(_contactfilter, _hitColliders);
        return hitCount > 0;
    }

    private void Update()
    {
        _animator.SetBool("Push", TestHit());
    }
}