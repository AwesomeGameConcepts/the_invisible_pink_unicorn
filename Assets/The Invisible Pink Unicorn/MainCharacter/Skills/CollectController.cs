using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectController : MonoBehaviour
{
    private ContactFilter2D _contactfilter;

    private Collider2D[] _hitColliders;

    [SerializeField]
    private Player _player;

    [SerializeField]
    private Collider2D _collider;

    private void Awake()
    {
        _contactfilter.useTriggers = true;
        _contactfilter.useLayerMask = true;
        _contactfilter.SetLayerMask(LayerMaskHelper.Collectable);

        _hitColliders = new Collider2D[1];
    }

    private void TestHit()
    {
        int hitCount = _collider.OverlapCollider(_contactfilter, _hitColliders);
        if (hitCount > 0)
        {
            var collectable = _hitColliders[0].GetComponent<Collectable>();
            if (collectable == null)
                return;

            _player.Collect(collectable);
        }
    }

    private void Update()
    {
        //TestHit();
    }
}