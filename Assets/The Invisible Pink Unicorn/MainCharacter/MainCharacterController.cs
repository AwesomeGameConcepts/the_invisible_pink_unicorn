using AGC;
using Assets.Ekioo.Framework.Data;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Inventory))]
[RequireComponent(typeof(Player))]
public class MainCharacterController : MonoBehaviour
{
    private Inventory _inventory;

    [SerializeField]
    private BooleanData _isOnGround;

    [SerializeField]
    private Rigidbody2D _rigidBody;

    [SerializeField] private LayerMask m_GroudLayer;

    private Vector2 _velocity;

    private Player _player;

    [SerializeField]
    private Animator _animator;

    private bool _isLiving;

    private static Vector3 m_V3Zero = Vector3.zero;

    public bool IsLiving
    {
        get => _isLiving; set
        {
            if (!value)
            {
                Die();
            }
            _isLiving = value;
        }
    }

    private void Die()
    {
        _rigidBody.velocity = m_V3Zero;
        _velocity = m_V3Zero;
        _animator.SetBool("IsGrounded", true);
        _animator.SetFloat("AbsoluteSpeed", Mathf.Abs(_velocity.x));
        GameCanvas.Instance.GameOver();
    }

    private void Start()
    {
        _player = GetComponent<Player>();
        _inventory = GetComponent<Inventory>();
        Respawn();
    }

    #region Health
    [SerializeField]
    private float _cooldownHit;

    private float _lastHit;
    private void DealDamages(int damage)
    {
        _animator.SetTrigger("Hit");
        _player.TakeDamage(damage);
    }

    public void HitPlayer(int damage)
    {
        if (Time.time - _lastHit < _cooldownHit)
            return;
        _lastHit = Time.time;
        DealDamages(damage);
    }
    #endregion

    #region CheckPoint and Respawn

    [System.NonSerialized] public Vector3 LastCheckpoint = m_V3Zero;

    public void TeleportToCheckPoint()
    {
        transform.position = LastCheckpoint != m_V3Zero ? LastCheckpoint : Map.Instance.SpawnPosition;
        Respawn();
    }

    public void TeleportToSpawn()
    {
        transform.position = Map.Instance.SpawnPosition;
        Respawn();
    }

    public void Respawn()
    {
        _isLiving = true;
        UpdateLastSecurePos();
    }

    #endregion CheckPoint and Respawn

    #region SecurePosition

    private Vector3 _lastSecurePosition;
    private float _updateTime = 1;

    private async void UpdateLastSecurePos()
    {
        while (_isLiving)
        {
            if (_isOnGround) _lastSecurePosition = transform.position;
            await new WaitForSeconds(_updateTime);
        }
    }

    public void TeleportToSecurePosition()
    {
        transform.position = _lastSecurePosition;
    }

    #endregion SecurePosition

    #region Jump

    [SerializeField]
    private FloatData _jumpDoubleForce;

    private bool _isDoubleJumping;
    private bool _isJumping;

    [SerializeField]
    private FloatData _jumpNormalForce;

    /// <summary>
    /// Indique si le joueur est en l'air et � d�j� saut� une fois, et une seule fois
    /// </summary>
    private bool _mayDoubleJump;

    private bool _wantToJump = false;

    public void OnJump(InputAction.CallbackContext context)
    {
        if (_inventory.CanJump)
            if (context.phase == InputActionPhase.Performed)
                _wantToJump = true;
    }

    private void DoubleJump()
    {
        if (_isOnGround && !_wantToJump)
        {
            _isDoubleJumping = false;
        }

        if (_mayDoubleJump && !_isDoubleJumping && _wantToJump)
        {
            _isDoubleJumping = true;
            _animator.SetTrigger("Jump");
            AudioManager.Instance.PlayChangeJumpSound();

            _velocity += _jumpDoubleForce * Vector2.up;
        }

        _mayDoubleJump = _isJumping && !_wantToJump;
    }

    private void Jump()
    {
        if (_isOnGround && !_wantToJump)
        {
            _isJumping = false;
        }

        if (_isOnGround && !_isJumping && _wantToJump)
        {
            _wantToJump = false;
            _isJumping = true;

            _animator.SetTrigger("Jump");
            AudioManager.Instance.PlayChangeJumpSound();

            _velocity += _jumpNormalForce * Vector2.up;
        }

        if (_isOnGround)
            _wantToJump = false;
    }

    #endregion Jump

    #region D�placement

    [SerializeField]
    private FloatData _acceleration;

    [SerializeField]
    private FloatData _deceleration;

    [SerializeField]
    private FloatData _maxMoveSpeed;

    private float _moveDirection;

    private float _lastFootStepPlay;

    public void OnMove(InputAction.CallbackContext context)
    {
        _moveDirection = context.ReadValue<float>();
    }

    private void Walk()
    {
        bool wantToMove = _moveDirection != 0;
        bool hasAVelocity = !(Mathf.Abs(_velocity.x) < 0.1f);

        bool wantToMoveInTheSameDirection = hasAVelocity && Mathf.Sign(_moveDirection) == Mathf.Sign(_velocity.x);

        if (wantToMove && (!hasAVelocity || wantToMoveInTheSameDirection))
        {
            _velocity.x += _moveDirection * Time.fixedDeltaTime * _acceleration;
            _velocity.x = Mathf.Clamp(_velocity.x, -_maxMoveSpeed, _maxMoveSpeed);
        }
        else
        {
            float sign = Mathf.Sign(_velocity.x);
            _velocity.x += -sign * Time.fixedDeltaTime * _deceleration;
            if (Mathf.Sign(_velocity.x) != sign)
                _velocity.x = 0;
        }
    }

    private void PlayFootSteps()
    {
        if (Mathf.Abs(_velocity.x) < 1)
            return;

        if (_isOnGround == false)
            return;

        if (Time.time - _lastFootStepPlay < 0.5f)
            return;

        _lastFootStepPlay = Time.time;
        AudioManager.Instance.PlayfootSteps();
    }

    #endregion D�placement

    // Update is called once per frame
    private void FixedUpdate()
    {

        _isOnGround.Value = CheckGround();

        _velocity = _rigidBody.velocity;

        Walk();

        if (_inventory.CanJump)
            Jump();

        if (_inventory.CanDoubleJump)
            DoubleJump();

        PlayFootSteps();

        if (_velocity.x < -_maxMoveSpeed)
            _velocity.x = -_maxMoveSpeed;

        if (_velocity.x > _maxMoveSpeed)
            _velocity.x = _maxMoveSpeed;

        _rigidBody.velocity = _velocity;

        _animator.SetFloat("AbsoluteSpeed", Mathf.Abs(_velocity.x));
        _animator.SetBool("IsGrounded", _isOnGround);

        Flip();
    }

    private void Flip()
    {
        if (_moveDirection < 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else if (_moveDirection > 0)
            transform.localScale = new Vector3(1, 1, 1);
    }

    private bool CheckGround()
    {
        return Physics2D.Raycast(_rigidBody.position + Vector2.up, Vector2.down, 1.3f, m_GroudLayer).collider != null;
    }
}