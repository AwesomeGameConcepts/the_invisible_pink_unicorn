//Invisible Pink Unicorn
->Introduction1

===StopStory===
#Story_Stop
""
->END

===Introduction1===
//image = laverie automatique avec linge empilé (non genré) dans le tambour avec une seule chaussette arc-en-ciel bien visible
#Slide_Next #Voice_Sam
Mais où est passée ma seconde chaussette arc-en-ciel ? 
Je ne peux pas me présenter à mon concours sans avoir ma paire de chaussettes arc-en-ciel porte bonheur. 
J’ai pourtant bien mis les deux dans le tambour tout à l’heure ! 
Mais comment c’est possible qu’il y ait autant de chaussettes qui disparaissent ?
#Voice_???
Ça c’est à cause de la Licorne Rose Invisible. 
C’est elle qui a pris la seconde chaussette. 
C'est pour dénoncer l’absurdité de ce monde. Celui de voir un signe du destin, là où il n’y en a pas.
#Voice_Sam
???
J'ai rien compris
...
Mais surtout ...
QUI ME PARLE ?
->Inroduction2


===Inroduction2===
//image = vitre de la laverie montrant l'étal de la poissonnerie à côté avec des poissons, coquillages et crustacés majoritairement gris sauf une huître violette
#Slide_Next #Voice_???
Ici !
De l'autre côté de la vitre
Je suis l’huître cachée dans l’étal du poissonnier. C’est un camouflage pour ne pas que l’on me voit
#Voice_Sam
Une huître violette ? tu parles d’un camouflage ! 
Et puis … 
COMMENT SE FAIT-IL QUE L'ON PUISSE PARLER ENSEMBLE ?
#Voice_HuîtreViolette
Je ne suis pas une huître ordinaire. Je fus au service de la Licorne Rose Invisible. 
Mais maintenant qu’elle m’a bannie, je te propose d’être la personne élue qui pourra la combattre. 
Ainsi tu retrouveras ta seconde chaussette. Mais grâce à toi, d’autres personnes aussi.
#Voice_Sam
...
Admettons.
Admettons que cette licorne existe. Comment je vais la retrouver alors que tu m’as dit qu’elle est INVISIBLE !
#Voice_HuîtreViolette
Je te donnerai cette aptitude en même temps que celle d’entrer dans le monde de la machine à laver. Mais sois tranquille, tu garderas ton aptitude à mettre des chaussettes.
#Voice_Sam
Entrer dans la machine à laver ?
Voir une licorne ? Rose et invisible ?
Mettre des chaussettes serait une aptitude ?
Je me demande si ce n’est pas plus étrange que de parler avec une huître violette.
#Voice_HuîtreViolette
Je prends cela pour un OUI
#Slide_Next
Hein ?
#Slide_Hide
//la séquence se finit par un visuel où le personnage est plongé dans le tambour dans la machine à laver. 
->Introduction3

===Introduction3===
//arrivée à l'entrée du monde de la Licorne Rose Invisible
#Voice_Sam
Je ne vais pas demander où je suis. Je crois avoir compris que tout ceci dépassait toute forme de logique. 
Mais tout de même. C’est quoi cette apparence ?
#Voice_HuîtreViolette
Tu as bien compris que tu étais dans le monde de la Licorne Rose Invisible. Ou tout du moins, dans la machine à laver. L’envers du décor. 
Quant à ton apparence, c’est mon choix. 
//Comme tu t'appelles SAM (SAM étant la personne qui ne boit pas) je te donne la possibilité d'incarner une personne réputée pour sa capacité à boire. Tu verras aussi les difficultés auxquelles sont confrontées les personnes de petite taille, comme moi.
//#Voice_Sam
//Et tout le monde porte la barbe. Ainsi on ne distingue pas homme et femme, comme pour mon prénom. J'ai compris.
#Voice_HuîtreViolette
Maintenant, je te propose de partir visiter ce monde pour arriver en face de la licorne et lui ferrer les sabots. 
Tu pourras récupérer ta chaussette et retourner dans le monde absurde d’où tu viens.

->Boxer

===Boxer===
#Voice_HuîtreViolette
En mettant ce boxer, tu pourras maintenant … boxer. 
#Voice_Sam
L’absurdité de ce monde n’a donc aucune limite
#Voice_HuîtreViolette
Il te suffit maintenant de toucher tes ennemis avant qu’ils ne te touchent. Essaie avec celui-là en appuyant sur la barre espace
->StopStory

===ChaussetteKangourou1===
#Voice_HuîtreViolette
Je t’avais dit que tu garderais l’aptitude à mettre des chaussettes
#Voice_Sam
Je n’avais pas compris que cela sous-entendrait que je perdais la capacité à porter mes autres vêtements. 
#Voice_HuîtreViolette
Ceci est une chaussette kangourou (reconnaissable à sa petite poche). Elle te permet maintenant de sauter.
#Voice_Sam
Cela me permet aussi d’être ridicule en ne portant qu’une seule chaussette
#Voice_HuîtreViolette
N'oublie pas d'enfiler ta chaussette en appuyant sur A.
Maintenant essaie de sauter en faisant avec Z. Tu vas pouvoir explorer d’autres parties du monde.
->StopStory

===ChaussetteKangourou2===
#Voice_HuîtreViolette
Tiens ! étrange. 
Une seconde chaussette kangourou. Normalement la Licorne Rose Invisible ne fait disparaître qu’une seule chaussette.
#Voice_Sam
Pourquoi cela ? 
… 
Je regrette déjà d’avoir posé la question
#Voice_HuîtreViolette
Les gens y voient là un signe et essaient de l'interpréter. Ce qui est assez illogique. 
C’est la manière que la Licorne Rose Invisible utilise pour dénoncer l’absurdité de votre monde.
#Voice_Sam
Ah bah oui, tiens, tout de suite ça fait sens
#Voice_HuîtreViolette
N’est ce pas? 
La seule explication que je vois c’est que deux personnes différentes ont perdu une chaussette kangourou.
#Voice_Sam
Ce qui veut dire qu’il y a au moins deux personnes dans le monde qui soient assez ridicules pour porter ces chaussettes.
#Voice_HuîtreViolette
Trois avec toi. 
#Voice_Sam
…
#Voice_HuîtreViolette
Peut-être que cela te permettra de sauter deux fois plus haut en appuyant deux fois sur Z. plus tu appuis rappidement plus tu sauteras haut ! Essaye maintenant.
->StopStory

===ChaussetteHercule1===
#Voice_HuîtreViolette
Oh la chance ! 
Une chaussette qui augmente la force, une chaussette Hercule
#Voice_Sam
Reconnaissable avec son poireau dessiné dessus. J’ai saisi la référence à Agatha Christie
#Voice_HuîtreViolette
Avec cela tu peux pousser des objets lourds
#Voice_Sam
Hélas je ne peux pas pousser ton humour
#Voice_HuîtreViolette
Essaie d'avancer lorsque tu es face à un obstacle comme une caisse, si tu t'en équipe tu pourra la pousser.//, une pile de linge ou un micro-onde à particules XZ20PF
//on peut changer les items annoncés en fonction de ce que les artistes ont mis comme item
->StopStory

===ChaussetteHercule2===
#Voice_HuîtreViolette
Deuxième chaussette Hercule ! Tu sais ce que cela veut dire ?
#Voice_Sam
J’ai peur de la réponse
#Voice_HuîtreViolette
Tu vas pouvoir pousser les objets trop lourds
#Voice_Sam
Ah bah pour une fois la réponse est logique. C’est surprenant.
#Voice_HuîtreViolette
Comme ces grosses caisses. Il suffit de tenter de les pousser avec les deux chaussetes équipées.
// encore une fois ce texte peut changer si vous optez pour le double clic
->StopStory

===ChaussettePutois1===
#Voice_Sam
Cette chaussette est collante !
#Voice_HuîtreViolette
C’est une chaussette du putois. Elle absorbe la transpiration. 
Ca colle et ça pue
#Voice_Sam
Mais c’est horrible !
#Voice_HuîtreViolette
Surtout pour les ennemies sur qui tu les jettes. 
Essaie d’en jeter une en utilisant le bon clic (gauche ou droit) selon où tu portes ta chaussette.
->StopStory

===ChaussettePutois2===
#Voice_Sam
Seconde chaussette putois ? Ben tiens. 
Ca me fait une belle jambe ... et des pieds qui puent.
#Voice_HuîtreViolette
Savais tu qu'en fait c'est la moufette qui est noire et blanche ? Pas le putois.
#Voice_Sam
J'ai renoncé à toute forme de logique ici. 
Et cette seconde chaussette me sert à quoi ?
#Voice_HuîtreViolette
A banlancer des bombes... des bombes puantes.
Essaie pour voir avec un clic (gauche comme droit).
//encore une fois, si vous avez opté pour le double clic
->StopStory

===ChaussetteSuper1===
#Voice_HuîtreViolette
Super une chaussette !
#Voice_Sam
Je ne vois rien d’exceptionnel ; il n’y a que de cela ici.
#Voice_HuîtreViolette
Non, je voulais dire : “waouh ! une chaussette super”
#Voice_Sam
Laisse moi deviner. Elle est sans plomb c’est ça ?
#Voice_HuîtreViolette
Peut-être. Mais surtout elle permet de planer
#Voice_Sam
Si c’est une blague elle n’est pas drôle
#Voice_HuîtreViolette
Jette toi dans le vide, tu verras bien.
->StopStory


===ChaussetteSuper2===
#Voice_HuîtreViolette
Tu sais que mon plus beau mensonge fut de faire rajouter des poivrons sur les pizzas ?
#Voice_Sam
Quel est le rapport avec cette seconde chaussette super ?
#Voice_HuîtreViolette
Maintenant tu vas pouvoir tirer des rayons laser
#Voice_Sam
C'est encore un mensonge ?
#Voice_HuîtreViolette
Comme pour la pizza, tu ne le sauras qu'en le tentant (clic)
//si c'est bien double clic
->StopStory

===StandPizza===
#Voice_HuîtreViolette
C’est un troll de maison. Lui aussi fait disparaître des objets mais dans toute la maison.
#Voice_Sam
On peut lui faire confiance ?
#Voice_HuîtreViolette
Absolument pas. Mais c’est le seul qui accepte toutes les pièces de monnaie perdues dans les machines à laver. Choisis juste bien ce que tu veux lui acheter
#Voice_Sam
Il n'y a que de la pizza mais avec du fruit dessus.
#Voice_HuîtreViolette
Pizza à l’ananas. C’est la préférée de la Licorne Rose Invisible. 
#Voice_Sam
C’est une pizza hawaïenne en fait ?
#Voice_HuîtreViolette
Et elle ne vient même pas d’Hawaï. Ce qui prouve que votre monde est bien plus absurde que le notre
#Voice_Sam
Et tu vas me dire que ça se mange ?
#Voice_HuîtreViolette
Oui. Si tu en manges, tu regagnes de la vitalité. Ne craint rien, il n’y a pas de risque d’indigestion.
Pour l'instant il ne vend que des pizza, à 15 pièces chaque part ! Appuis sur la barre espace devant le chariot pour en acheter.
//on peut retoucher cette dernière phrase en fonction de comment se gère les part de pizza et si on peut s'en servir comme appât contre la licorne.
->StopStory

===StandPizza1===
#Voice_Gobelin
Bonjour voyageur d'une autre dimmension que puis-je faire pour vous ?
#Voice_Sam
Bonjour, j'ai perdu ma chaussette et il semblerais que ce soit la licorne qui me l'a volée.
#Voice_Gobelin
Ha encore une autre victime, on comprend mieux pourquois il y a tant de chaussettes dépareillés dans votre monde.
J'ai vu la licorne passer par ici il y a quelques minutes. Demande à mon frère un peu plus loin il l'a peut être vu...
#Voice_Sam
Trés bien merci
->StopStory

===FinTmp===
#Voice_Sam
Salut, c'est toi que je dois rencontrer ?
#Voice_Gobelin
Bonjour, oui c'est moi, j'ai quelque chose d'important à te dire.
C'est la fin du niveau ! Il va falloir attendre la prochaine mise à jour avec la chausette superman et le combat contre la licorne !
#Voice_Sam
Hein !?!
Ça ne peut pas finir comme cela, je n'ai pas récupérer ma chausette !
#Voice_Gobelin
Je comprend mais le développement d'un jeu prend du temps...
Si tu souhaite continuer il va falloir que tu dise au joueur qu'il manifeste son soutien au projet !
#Voice_Sam
Comment doit-il s'y prendre ?
#Voice_Gobelin
Il peu déjà suivre la page Itch, et laisser un feedback !
#Voice_Sam
Très bien, bon tu sais ce qu'il te reste à faire ! jusqu'à présent tu m'a bien guidé, mais j'ai encore besoin de toi pour trouver cette satané chaussette !
Merci d'avoir jouer.
->END

===Licorne===
#Voice_HuîtreViolette
Ca y est ! 
La voilà. 
Elle est là ! 
Tu la vois ?
#Voice_Sam
Comment pourrais-je la rater. T’as vu la taille ! Comment veux tu que je la combatte ?
#Voice_HuîtreViolette
Repense à tout ce que tu as vu. Normalement tu es capable de la toucher au moins une fois. 
Mais je ne sais pas combien de fois tu auras besoin de le faire.
#Voice_Sam
Mais c’est complètement ridicule
#Voice_HuîtreViolette
C’est toi qui fut assez ridicule pour venir ici juste pour une chaussette.
Bonne chance. Moi je vais préparer une pizza aux poivrons.
#Voice_Sam
HHEEEYYY !!!
#Story_Stop
""
->DONE

===Fin===
#Voice_Sam
J'ai enfin pu revenir dans mon monde et j'ai pu récupérer ma chaussette.
Demain j'irai à mon concours mais sans ma paire porte bonheur.
S'il y a bien une chose que cette aventure m'a enseignée, c'est d'éviter dorénavant les absurdités de ce monde.
#Voice_HuîtreViolette
Etre superstitieux, ça porte malheur
#Story_Stop
""
->DONE
