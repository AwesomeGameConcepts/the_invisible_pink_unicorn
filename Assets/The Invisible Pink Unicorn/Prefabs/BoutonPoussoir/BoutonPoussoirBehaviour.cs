using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoutonPoussoirBehaviour : MonoBehaviour
{
    private Vector3 _initialPosition;

    [SerializeField]
    private float _speed = 0.02f;

    [SerializeField]
    private Vector3 _pushedPosition;

    [SerializeField]
    private Collider2D _collider;

    public UnityEvent OnPushed = new UnityEvent();

    public UnityEvent OnReleased = new UnityEvent();

    [SerializeField]
    private bool _isPushed;

    [SerializeField]
    public float _pushedThreshold = 0.8f;

    // Start is called before the first frame update
    private void Awake()
    {
        _initialPosition = transform.position;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        bool hit = _collider.IsTouchingLayers();
        if (hit)
        {
            transform.position = Vector3.MoveTowards(transform.position, _initialPosition + _pushedPosition, _speed);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, _initialPosition, _speed);
        }

        float distanceToPushedState = Vector3.Distance(transform.position, _initialPosition + _pushedPosition);

        // si le bouton est pouss� � _pushedThreshold %
        bool newIsPushedState = (_pushedPosition.magnitude - distanceToPushedState) / _pushedPosition.magnitude > _pushedThreshold;

        if (_isPushed != newIsPushedState)
        {
            _isPushed = newIsPushedState;

            if (_isPushed)
                OnPushed?.Invoke();
            else
                OnReleased?.Invoke();
        }
    }
}