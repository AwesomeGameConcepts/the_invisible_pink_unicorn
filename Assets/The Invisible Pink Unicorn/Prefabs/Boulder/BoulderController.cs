using Assets.Ekioo.Framework.Data;
using UnityEngine;

public class BoulderController : MonoBehaviour
{
    [SerializeField]
    private IntData _pushUse;

    [SerializeField]
    private FloatData _boulderMassNoPush;

    [SerializeField]
    private FloatData _boulderMassPushLight;

    [SerializeField]
    private FloatData _boulderMassPushStrong;

    private Rigidbody2D _rigidbody;
    private Vector3 _initialPos;
    private Quaternion _initialRot;

    public void Respawn()
    {
        transform.position = _initialPos;
        transform.rotation = _initialRot;
    }

    private void Start()
    {
        _initialPos = transform.position;
        _initialRot = transform.rotation;
        _rigidbody = GetComponent<Rigidbody2D>();
        _pushUse.OnValueChanged += _pushUse_OnValueChanged;

        _rigidbody.mass = _boulderMassNoPush.Value;
    }

    private void OnDestroy()
    {
        _pushUse.OnValueChanged -= _pushUse_OnValueChanged;
    }

    private void _pushUse_OnValueChanged(int value)
    {
        if (value == 0)
            _rigidbody.mass = _boulderMassNoPush.Value;
        else if (value == 1)
            _rigidbody.mass = _boulderMassPushLight.Value;
        else if (value == 2)
            _rigidbody.mass = _boulderMassPushStrong.Value;
    }
}