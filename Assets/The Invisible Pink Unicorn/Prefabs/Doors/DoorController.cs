using AGC;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField]
    private bool _isLock;

    [SerializeField]
    private Animator _animator;

    private void Awake()
    {
        _animator.SetBool("IsLock", _isLock);
    }

    public void Lock()
    {
        ChangeState(true);
        AudioManager.Instance.PlayCloseDoor();
    }

    public void Unlock()
    {
        ChangeState(false);
        AudioManager.Instance.PlayOpenDoor();
    }

    public void ChangeState(bool newState)
    {
        if (_isLock != newState)
        {
            _isLock = newState;

            _animator.SetBool("IsLock", _isLock);
        }
    }
}