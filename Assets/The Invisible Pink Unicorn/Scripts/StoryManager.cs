﻿using UnityEngine;
using Ink.Runtime;

public class StoryManager : MonoBehaviour
{
    [SerializeField] private TextAsset m_StoryAsset;
    [SerializeField] private DialogWindow m_Dialog;

    private Story m_Story;
    private string m_Speaker;
    public bool IsReadingTag { get; private set; } = false;

    private void Awake()
    {
        m_Story = new Story(m_StoryAsset.text);
    }

    public string SpeakerName
    {
        set
        {
            if (value != m_Speaker)
            {
                m_Speaker = value;
                if (value == "None") m_Dialog.SetNoSpeaker();
                else if (value == GameCanvas.Instance.Player.Name) m_Dialog.SetSpeakerPlayer(value);
                else m_Dialog.SetSpeakerNPC(value);
            }
        }
    }

    public bool StoryCanContinue => m_Story.canContinue;
    public bool StoryHasChoice => m_Story.currentChoices.Count > 0;

    public void SetKnot(string knot)
    {
        if (m_Story != null)
            m_Story.ChoosePathString(knot);
    }

    private void ReadTags()
    {
        IsReadingTag = true;
        foreach (string tag in m_Story.currentTags)
        {
            string action = tag.Split('_')[0];
            string value;
            string name;
            //Debug.Log("read " + tag);
            switch (action)
            {
                case "Voice":
                    value = tag.Split('_')[1];
                    name = value.Split('-')[0];
                    SpeakerName = name;
                    //if (value != m_Speaker) AudioManager.Instance.PlayVoice(value);
                    break;

                case "Story":
                    value = tag.Split('_')[1];
                    switch (value)
                    {
                        case "Stop":
                            GameCanvas.Instance.StopDialog();
                            break;

                        case "Continue":
                            m_Dialog.ContinueStory();
                            break;
                    }
                    break;

                case "Slide":
                    value = tag.Split('_')[1];
                    if (value == "Next") GameCanvas.Instance.NextStorySlide();
                    else GameCanvas.Instance.HideStorySlide();
                    break;

                case "Tutorial":
                    break;
            }
        }
        IsReadingTag = false;
    }

    public void DisplayDialog()
    {
        //AudioManager.Instance.StopDialogVoiceAndSFX();
        m_Dialog.DialogText = m_Story.Continue();
        ReadTags();
    }
}