﻿using AGC;
using System.Collections;
using UnityEngine;
public class Map : MonoBehaviour
{
    public static Map Instance;
    [SerializeField] private Transform m_SpawnTransform;
    [SerializeField] private Transform m_CoinParent;
    [SerializeField] private Transform m_SockParent;
    [SerializeField] private Transform m_BoulderParent;
    [SerializeField] private Transform m_DoorParent;

    [SerializeField] private Transform m_EnemiesParent;
    [SerializeField] private float m_FadeInAudio = 5;
    public Vector3 SpawnPosition => m_SpawnTransform.position;

    private async void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this) Destroy(gameObject);
    }

    private void Start()
    {
        AudioManager.Instance.PlayIntroGameMusic(m_FadeInAudio);
    }

    public void ResetMap()
    {
        for (int i = 0; i < m_CoinParent.childCount; i++)
        {
            m_CoinParent.GetChild(i).GetComponent<Coin>().Reset();
        }
        for (int i = 0; i < m_SockParent.childCount; i++)
        {
            m_SockParent.GetChild(i).GetComponent<Sock>().Reset();
        }
        for (int i = 0; i < m_EnemiesParent.childCount; i++)
        {
            m_EnemiesParent.GetChild(i).GetChild(0).GetComponent<Enemy>().Respawn();
        }
        for (int i = 0; i < m_BoulderParent.childCount; i++)
        {
            m_BoulderParent.GetChild(i).GetComponent<BoulderController>().Respawn();
        }
        for (int i = 0; i < m_DoorParent.childCount; i++)
        {
            m_DoorParent.GetChild(i).GetComponent<DoorController>().Lock();
        }
    }
}