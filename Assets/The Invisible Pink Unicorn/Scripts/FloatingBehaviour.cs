﻿using UnityEngine;

public class FloatingBehaviour : MonoBehaviour
{
    private Vector3 _initialPosition;

    private float _offset;

    private void Awake()
    {
        _offset = Random.value;
        _initialPosition = transform.position;
    }

    private void Update()
    {
        transform.position = _initialPosition + Vector3.up * Mathf.Sin(2 * Time.time + _offset) * 0.3f;
    }
}