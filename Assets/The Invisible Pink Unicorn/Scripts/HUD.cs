﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_MoneyAmount;
    [SerializeField] private TextMeshProUGUI m_MedikitAmount;
    [SerializeField] private TextMeshProUGUI m_MaxMoneyAmount;
    [SerializeField] private TextMeshProUGUI m_MaxMedikitAmount;
    [SerializeField] private TextMeshProUGUI m_Score;
    [SerializeField] private Image m_LeftSock;
    [SerializeField] private Image m_RightSock;
    [SerializeField] private Image[] m_Life;
    [SerializeField] private Sprite m_NoSockSprite;
    [SerializeField] private Sprite m_FilledLifeSprite;
    [SerializeField] private Sprite m_EmptyLifeSprite;
    private void Reset()
    {
        m_MoneyAmount.text = "0";
        m_MedikitAmount.text = "0";
        m_LeftSock.sprite = m_NoSockSprite;
        m_RightSock.sprite = m_NoSockSprite;

    }

    // Use this for initialization
    void Start()
    {

    }
    public void SetMax(int maxMoney, int maxMedikit)
    {
        m_MaxMoneyAmount.text = "/ " + maxMoney;
        m_MaxMedikitAmount.text = "/ " + maxMedikit;
    }

    public void UpdateMoney(int value)
    {
        m_MoneyAmount.text = value.ToString();
    }

    public void UpdateMedikit(int value)
    {
        m_MedikitAmount.text = value.ToString();
    }

    public void UdpateScore(int value)
    {
        m_Score.text = value.ToString();
    }

    public void UpdateLeftSock(Sprite s)
    {
        m_LeftSock.sprite = s !=null ? s : m_NoSockSprite;
    }
    public void UpdateRightSock(Sprite s)
    {
        m_RightSock.sprite = s != null ? s : m_NoSockSprite;
    }

    public void UpdateLife(int value)
    {
        for (int i = 0; i < m_Life.Length; i++)
        {
            m_Life[i].sprite = i < value  ? m_FilledLifeSprite : m_EmptyLifeSprite;
        }
    }
}