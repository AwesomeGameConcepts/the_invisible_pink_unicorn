﻿using UnityEngine;
using UnityEditor;
namespace AGC
{
    public class SelectionUtilityWindows : EditorWindow
    {
        [SerializeField] private Vector3 m_Value = default;

        [MenuItem("AGC/Windows/Selection Utility")]
        static void OpenWindow()
        {
            SelectionUtilityWindows window = GetWindow(typeof(SelectionUtilityWindows), false, "Selection Utility") as SelectionUtilityWindows;
            window.Show();
        }
        private void OnGUI()
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("m_Value");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();
            GUILayout.Label("Position");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add")) AddToPos(serializedProperty.vector3Value);
            if (GUILayout.Button("Substract")) SubstractToPos(serializedProperty.vector3Value);
            if (GUILayout.Button("Multiply")) MultiplyToPos(serializedProperty.vector3Value);
            if (GUILayout.Button("Divide")) DivideToPos(serializedProperty.vector3Value);
            GUILayout.EndHorizontal();
            GUILayout.Label("Rotation");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add")) AddToRot(serializedProperty.vector3Value);
            if (GUILayout.Button("Substract")) SubstractToRot(serializedProperty.vector3Value);
            if (GUILayout.Button("Multiply")) MultiplyToRot(serializedProperty.vector3Value);
            if (GUILayout.Button("Divide")) DivideToRot(serializedProperty.vector3Value);
            GUILayout.EndHorizontal();
            GUILayout.Label("Scale");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add")) AddToScale(serializedProperty.vector3Value);
            if (GUILayout.Button("Substract")) SubstractToScale(serializedProperty.vector3Value);
            if (GUILayout.Button("Multiply")) MultiplyToScale(serializedProperty.vector3Value);
            if (GUILayout.Button("Divide")) DivideToScale(serializedProperty.vector3Value);
            GUILayout.EndHorizontal();
            GUILayout.Label("Apply To Children"); 
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Position")) ApplyTranslateToChildren();
            if (GUILayout.Button("Rotation")) ApplyRotationToChildren();
            if (GUILayout.Button("Scale")) ApplyScaleToChildren();
            GUILayout.EndHorizontal();
            GUILayout.Label("Enemy");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Flip")) FlipEnemy();
            if (GUILayout.Button("SetPatrolPoint")) SetPatrolPoint();
            //if (GUILayout.Button("other")) ApplyScaleToChildren();
            GUILayout.EndHorizontal();
        }

        #region Position

        private void AddToPos(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localPosition += val;
            }
        }

        private void SubstractToPos(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localPosition -= val;
            }
        }

        private void MultiplyToPos(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localPosition = Selected[i].transform.localPosition.Multiply(val);
            }
        }

        private void DivideToPos(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localPosition = Selected[i].transform.localPosition.Divide(val);
            }
        }

        private void ApplyTranslateToChildren()
        {
            foreach (Transform selected in Selection.GetTransforms(SelectionMode.TopLevel))
            {
                for (int i = 0; i < selected.childCount; i++)
                {
                    selected.GetChild(i).localPosition += selected.localPosition;
                }
                selected.localPosition = Vector3.zero;
            }

        }
        #endregion
        #region Scale
        private void AddToScale(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localScale += val;
            }
        }

        private void SubstractToScale(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localScale -= val;
            }
        }

        private void MultiplyToScale(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localScale = Selected[i].transform.localScale.Multiply(val);
            }
        }

        private void DivideToScale(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localScale = Selected[i].transform.localPosition.Divide(val);
            }
        }

        private void ApplyScaleToChildren()
        {
            foreach (Transform selected in Selection.GetTransforms(SelectionMode.TopLevel))
            {
                for (int i = 0; i < selected.childCount; i++)
                {
                    selected.GetChild(i).localScale = selected.GetChild(i).localScale.Multiply(selected.localScale);
                }
                selected.localScale = Vector3.one;
            }

        }
        #endregion
        #region Rotation
        private void AddToRot(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localRotation = Quaternion.Euler(Selected[i].transform.localRotation.eulerAngles + val);
            }
        }

        private void SubstractToRot(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localRotation = Quaternion.Euler(Selected[i].transform.localRotation.eulerAngles - val);
            }
        }

        private void MultiplyToRot(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localRotation = Quaternion.Euler(Selected[i].transform.localRotation.eulerAngles.Multiply(val));
            }
        }

        private void DivideToRot(Vector3 val)
        {
            GameObject[] Selected = Selection.gameObjects;
            for (int i = 0; i < Selected.Length; i++)
            {
                Selected[i].transform.localRotation = Quaternion.Euler(Selected[i].transform.localRotation.eulerAngles.Divide(val));
            }
        }
        private void ApplyRotationToChildren()
        {
            Debug.LogError("not eplemented yet");
            //foreach (Transform selected in Selection.GetTransforms(SelectionMode.TopLevel))
            //{
            //    for (int i = 0; i < selected.childCount; i++)
            //    {
            //        //selected.GetChild(i).localPosition += selected.localPosition;
            //    }
            //    //selected.localPosition = Vector3.zero;
            //}

        }
        #endregion

        #region Enemy
        private void FlipEnemy()
        {
            //foreach (Transform selected in Selection.GetTransforms(SelectionMode.TopLevel))
            //{
            //    if (selected.GetComponent<Enemy>()) selected.GetComponent<Enemy>();
            //}
        }

        private void SetPatrolPoint()
        {

        }
        #endregion
    }
}