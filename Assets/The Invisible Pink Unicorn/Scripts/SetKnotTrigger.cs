﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(Collider2D))]
public class SetKnotTrigger : MonoBehaviour
{
    [SerializeField] private string m_knot;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            GameCanvas.Instance.DialogWindow.SetInkKnot(m_knot);
            Destroy(this);
        }
    }
}