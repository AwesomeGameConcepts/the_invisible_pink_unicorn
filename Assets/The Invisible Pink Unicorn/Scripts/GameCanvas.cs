﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

internal class GameCanvas : MonoBehaviour
{
    public static GameCanvas Instance;

    [SerializeField] private HUD m_HUD;
    [SerializeField] private DialogWindow m_Dialog;
    [SerializeField] private Player m_Player;

    [SerializeField] private bool m_SkipDialog;

    [SerializeField] private Image m_StoryImage;
    [SerializeField] private Sprite[] m_StorySlide;

    [SerializeField] private GameObject m_GameOverMenu;
    [SerializeField] private GameObject m_PauseMenu;
    [SerializeField] private GameObject m_FnishMenu;

    private int m_StorySlideIndex = -1;

    public HUD HUD => m_HUD;

    public DialogWindow DialogWindow => m_Dialog;

    public Player Player => m_Player;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this) Destroy(gameObject);
    }

    private void Start()
    {
        if (!m_SkipDialog) StartDialog();
    }

    public void GameOverMenuContinue()
    {
        m_Player.GetComponent<MainCharacterController>().TeleportToCheckPoint();
        m_Player.ResetLife();
        m_GameOverMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void GameOverMenuQuit()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void PauseMenuContinue()
    {
        Time.timeScale = 1;
        m_PauseMenu.SetActive(false);
    }

    public void OpenPauseMenu(InputAction.CallbackContext context)
    {

        if (context.phase != InputActionPhase.Performed)
            return;
        if (!m_Dialog.gameObject.activeSelf)
        {
            if (m_PauseMenu.activeSelf)
            {
                Time.timeScale = 1;
                m_PauseMenu.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                m_PauseMenu.SetActive(true);
            }
        }
    }

    public void GameOverMenuRetry()
    {
        Map.Instance.ResetMap();
        m_Player.GetComponent<MainCharacterController>().TeleportToSpawn();
        m_Player.Reset();
        m_GameOverMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void GameOver()
    {
        m_GameOverMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void ShowFinishMenu()
    {
        m_FnishMenu.SetActive(true);
        Time.timeScale = 0;
        Debug.Log(Time.timeScale);
    }

    public void OnSkipDialog(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed)
            return;

        if (m_Dialog.gameObject.activeSelf)
            m_Dialog.ContinueStory();
    }

    public void StartDialog()
    {
        if (!m_SkipDialog)
        {
            m_HUD.gameObject.SetActive(false);
            m_Dialog.gameObject.SetActive(true);
            m_Dialog.ContinueStory();
        }
    }

    public void StopDialog()
    {
        m_HUD.gameObject.SetActive(true);
        m_Dialog.gameObject.SetActive(false);
    }

    public void NextStorySlide()
    {
        m_StoryImage.gameObject.SetActive(true);
        m_StorySlideIndex = (m_StorySlideIndex + 1) % m_StorySlide.Length;
        m_StoryImage.sprite = m_StorySlide[m_StorySlideIndex];
    }

    public void HideStorySlide()
    {
        m_StoryImage.gameObject.SetActive(false);
    }
}