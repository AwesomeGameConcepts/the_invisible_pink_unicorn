﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using Assets.Ekioo.Framework.Data;
using System.Threading.Tasks;

public class DialogWindow : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_DialogText = default;
    [SerializeField] private GameObject m_DialogPanel = default;
    [SerializeField] private GameObject m_SpeakerNameTag = default;

    [SerializeField]
    private StoryManager m_StoryManager;

    private float m_InitialTagPositionX;

    private void OnEnable()
    {
        Time.timeScale = 0;
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
    }

    public void Awake()
    {
        m_InitialTagPositionX = m_SpeakerNameTag.GetComponent<RectTransform>().anchoredPosition.x;
    }

    public string DialogText
    {
        get => m_DialogText.text; set
        {
            m_DialogText.text = value;
        }
    }

    public void SetSpeakerPlayer(string name)
    {
        m_SpeakerNameTag.GetComponentInChildren<TextMeshProUGUI>().text = name;
        m_SpeakerNameTag.GetComponent<RectTransform>().anchoredPosition = new Vector2(-m_InitialTagPositionX, m_SpeakerNameTag.GetComponent<RectTransform>().anchoredPosition.y);
        m_SpeakerNameTag.SetActive(true);
    }

    public void SetSpeakerNPC(string name)
    {
        m_SpeakerNameTag.GetComponentInChildren<TextMeshProUGUI>().text = name;
        m_SpeakerNameTag.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_InitialTagPositionX, m_SpeakerNameTag.GetComponent<RectTransform>().anchoredPosition.y);
        m_SpeakerNameTag.SetActive(true);
    }

    public void SetNoSpeaker()
    {
        m_SpeakerNameTag.SetActive(false);
    }

    public void SetInkKnot(string knot)
    {
        m_StoryManager.SetKnot(knot);
        GameCanvas.Instance.StartDialog();
    }

    public async void ContinueStory()
    {
        if (m_StoryManager.StoryCanContinue)
        {
            /*if (m_ChoicesPanel.activeSelf)
            {
                ResetChoices();
            }*/
            m_DialogPanel.SetActive(true);
            m_StoryManager.DisplayDialog();
        }
        else if (m_StoryManager.StoryHasChoice)
        {
            m_DialogPanel.SetActive(false);
            //m_Choices = StoryManager.Instance.ReadChoice();
            //DisplayChoice();
            //m_ChoicesPanel.SetActive(true);
        }
        else //fin du ink
        {
            GameCanvas.Instance.StopDialog();
            GameCanvas.Instance.ShowFinishMenu();
            /*GameCanvas.Instance.StartSceneTransition("Fin du jeu", null).WrapErrors();
            await new WaitUntil(() => !GameCanvas.Instance.ChangeSceneTransitionActive);
            GameCanvas.Instance.Credit();*/
        }
    }
}