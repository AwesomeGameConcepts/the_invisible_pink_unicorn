﻿using System.Collections;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    [SerializeField] private Player m_Player;
    [SerializeField] private GameObject[] m_PlayerSocks;
    [SerializeField] private Transform m_Pool;

    [SerializeField] private Transform m_spawn;

    // Use this for initialization
    private async void Start()
    {
        WaitForSeconds Wait1 = new WaitForSeconds(1);
        await Wait1;
        GameObject instance;
        GameObject[] socks = new GameObject[m_PlayerSocks.Length];
        for (int i = 0; i < m_PlayerSocks.Length; i++)
        {
            socks[i] = Instantiate(m_PlayerSocks[i], m_Pool);
            socks[i].SetActive(true);
        }
        m_PlayerSocks = socks;
        await Wait1;

        for (int i = 0; i < m_PlayerSocks.Length; i++)
        {
            m_Player.Collect(m_PlayerSocks[i].GetComponent<Sock>());
        }
       
        if (m_spawn != null) m_Player.transform.position = m_spawn.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
}