﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Threading;
using System.Threading.Tasks;

namespace AGC
{
    public class AudioManager : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private AudioListener m_Listener = default;

        [SerializeField] private AudioMixer m_Mixer = default;
        [SerializeField] private AudioSource m_MusicSource = default;
        [SerializeField] private AudioSource m_SFXSource = default;
        [SerializeField] private AudioSource m_UISource = default;
        [SerializeField] private AudioSource m_VoicesSource = default;

        [Header("Variables")]
        [SerializeField] private float m_MinVolume = -80;

        [SerializeField] private float m_MaxVolume = 0;

        [Header("Exposed Parameters Names")]
        [SerializeField] private string m_MasterVolume = "MasterVolume";

        [SerializeField] private string m_MusicVolume = "MusicVolume";
        [SerializeField] private string m_SFXVolume = "SFXVolume";
        [SerializeField] private string m_UIVolume = "UIVolume";
        [SerializeField] private string m_VoicesVolume = "VoicesVolume";

        [Header("Music")]
        [SerializeField] private AudioClip m_MainMenu = default;

        [SerializeField] private AnimationCurve m_GameIntroFadeInCurve = default;
        [SerializeField] private int m_FadeInIteration = default;
        [SerializeField] private AudioClip m_Game = default;
        [SerializeField] private AudioClip m_Credits = default;

        [Header("SFX")]
        [SerializeField] private AudioClip m_CollectSound = default;
        [SerializeField] private AudioClip m_CollectCoin = default;
        [SerializeField] private AudioClip m_LaserSound = default;
        [SerializeField] private AudioClip m_MedikitUseSound = default;
        [SerializeField] private AudioClip m_BuySound = default;
        [SerializeField] private AudioClip m_JumpSound = default;
        [SerializeField] private AudioClip m_FightSound = default;
        [SerializeField] private AudioClip m_AirFightSound = default;
        [SerializeField] private AudioClip m_OpenDoor = default;
        [SerializeField] private AudioClip m_CloseDoor = default;

        [SerializeField] private AudioClip[] m_footSteps = default;

        [Header("UI")]
        [SerializeField] private AudioClip m_DialogButtonClick = default;

        [SerializeField] private AudioClip m_ButtonClick = default;

        [Header("Voices")]
        [SerializeField] private NamedAudio[] m_Voices = default;

        private CancellationTokenSource m_Cancel;
        public static AudioManager Instance;
        public AudioListener Listener => m_Listener;
        public AudioSource UiSource => m_UISource;

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else if (Instance != this) Destroy(gameObject);
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            UpdateVolume(1, 1, 1, 1, 1);
        }

        public void StopDialogVoiceAndSFX()
        {
            m_VoicesSource.Stop();
            m_SFXSource.Stop();
        }

        #region Volume

        public void UpdateVolume(float master, float music, float sfx, float ui, float voices)
        {
            MasterVolume = master;
            MusicVolume = music;
            SFXVolume = sfx;
            UIVolume = ui;
            VoicesVolume = voices;
        }

        /// <summary>
        /// get or set master volume by percent
        /// </summary>
        public float MasterVolume
        {
            get
            {
                float volume;
                m_Mixer.GetFloat(m_MasterVolume, out volume);
                return MathfExtensions.GetPercent(volume, m_MinVolume, m_MaxVolume);
            }
            set
            {
                m_Mixer.SetFloat(m_MasterVolume, MathfExtensions.GetValueByPercent(value, m_MinVolume, m_MaxVolume));
            }
        }

        /// <summary>
        /// get or set music volume by percent
        /// </summary>
        public float MusicVolume
        {
            get
            {
                float volume;
                m_Mixer.GetFloat(m_MusicVolume, out volume);
                return MathfExtensions.GetPercent(volume, m_MinVolume, m_MaxVolume);
            }
            set
            {
                m_Mixer.SetFloat(m_MusicVolume, MathfExtensions.GetValueByPercent(value, m_MinVolume, m_MaxVolume));
            }
        }

        /// <summary>
        /// get or set SFX volume by percent
        /// </summary>
        public float SFXVolume
        {
            get
            {
                float volume;
                m_Mixer.GetFloat(m_SFXVolume, out volume);
                return MathfExtensions.GetPercent(volume, m_MinVolume, m_MaxVolume);
            }
            set
            {
                m_Mixer.SetFloat(m_SFXVolume, MathfExtensions.GetValueByPercent(value, m_MinVolume, m_MaxVolume));
            }
        }

        /// <summary>
        /// get or set UI volume by percent
        /// </summary>
        public float UIVolume
        {
            get
            {
                float volume;
                m_Mixer.GetFloat(m_UIVolume, out volume);
                return MathfExtensions.GetPercent(volume, m_MinVolume, m_MaxVolume);
            }
            set
            {
                m_Mixer.SetFloat(m_UIVolume, MathfExtensions.GetValueByPercent(value, m_MinVolume, m_MaxVolume));
            }
        }

        /// <summary>
        /// get or set Voices volume by percent
        /// </summary>
        public float VoicesVolume
        {
            get
            {
                float volume;
                m_Mixer.GetFloat(m_VoicesVolume, out volume);
                return MathfExtensions.GetPercent(volume, m_MinVolume, m_MaxVolume);
            }
            set
            {
                m_Mixer.SetFloat(m_VoicesVolume, MathfExtensions.GetValueByPercent(value, m_MinVolume, m_MaxVolume));
            }
        }

        #endregion Volume

        #region PlaySound

        #region PlayMusic

        public void PlayMainMenuMusic()
        {
            m_MusicSource.clip = m_MainMenu;
            m_MusicSource.Play();
        }

        public void PlayIntroGameMusic(float introDuration)
        {
            m_MusicSource.clip = m_Game;
            m_Cancel = new CancellationTokenSource();
            m_MusicSource.volume = 0f;
            AudioAsync.CurveFadeInRealTimeAsync(m_MusicSource, m_GameIntroFadeInCurve, introDuration, m_FadeInIteration, CancelAction.Final, m_Cancel.Token).WrapErrors();
            m_MusicSource.Play();
        }

        public void InterruptGameMusicFadeIn()
        {
            m_Cancel.Cancel();
        }

        public void PlayCreditMusic()
        {
            m_MusicSource.clip = m_Credits;
            m_MusicSource.Play();
        }

        #endregion PlayMusic

        #region PlayUI

        public void PlayClickButtonSound()
        {
            m_UISource.clip = m_ButtonClick;
            m_UISource.Play();
        }

        public void PlayClickDialogButtonSound()
        {
            m_UISource.clip = m_DialogButtonClick;
            m_UISource.Play();
        }

        #endregion PlayUI

        #region PlaySFX

        public void PlayCollectSound()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_CollectSound;
            m_SFXSource.Play();
        }

        public void PlayCollectCoin()
        {
            m_SFXSource.clip = m_CollectCoin;
            m_SFXSource.pitch = Random.Range(0.9f, 1.1f);
            m_SFXSource.Play();
        }
        public void PlayBuySound()
        {
            m_SFXSource.clip = m_BuySound;
            m_SFXSource.pitch = 1;
            m_SFXSource.Play();
        }
        public void PlayChangeLaserSound()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_LaserSound;
            m_SFXSource.Play();
        }

        public void PlayChangeJumpSound()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_JumpSound;
            m_SFXSource.Play();
        }

        public void PlayChangeFightSound()
        {
            m_SFXSource.pitch = Random.Range(0.9f, 1.1f);
            m_SFXSource.clip = m_FightSound;
            m_SFXSource.Play();
        }

        public void PlayChangeAirFightSound()
        {
            m_SFXSource.pitch = Random.Range(0.9f, 1.1f);
            m_SFXSource.clip = m_AirFightSound;
            m_SFXSource.Play();
        }

        public void PlayUseMedikitSound()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_MedikitUseSound;
            m_SFXSource.Play();
        }
        public void PlayfootSteps()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_footSteps.Random();
            m_SFXSource.Play();
        }
        public void PlayOpenDoor()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_OpenDoor;
            m_SFXSource.Play();
        }
        public void PlayCloseDoor()
        {
            m_SFXSource.pitch = 1;
            m_SFXSource.clip = m_CloseDoor;
            m_SFXSource.Play();
        }

        #endregion PlaySFX

        public void PlayVoice(string soudname)
        {
            foreach (NamedAudio namedAudio in m_Voices)
            {
                if (namedAudio.Name == soudname)
                {
                    m_VoicesSource.clip = namedAudio.Clip;
                    m_VoicesSource.Play();
                    return;
                }
            }
            //Debug.LogError("Can't find sound " + soudname + " in Sound");
        }

        #endregion PlaySound
    }
}