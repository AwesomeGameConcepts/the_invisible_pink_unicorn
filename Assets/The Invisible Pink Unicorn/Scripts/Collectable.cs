﻿using UnityEngine;

public abstract class Collectable : MonoBehaviour
{
    [SerializeField] private int m_ScoreValue;

    public int ScoreValue => m_ScoreValue;
    public void Collect()
    {
        gameObject.SetActive(false);
        //play collect sound
    }

    protected virtual void Awake()
    {
        gameObject.AddComponent<FloatingBehaviour>();
    }

    public void Reset()
    {
        gameObject.SetActive(true);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>()) collision.GetComponent<Player>().Collect(this);
    }
}