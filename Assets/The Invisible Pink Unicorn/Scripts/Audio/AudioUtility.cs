﻿using UnityEngine;
namespace AGC
{
    public class AudioUtility : MonoBehaviour
    {
        public void PlayClickButtonSound()
        {
            AudioManager.Instance.PlayClickButtonSound();
        }
        public void PlayClickDialogButtonSound()
        {
            AudioManager.Instance.PlayClickDialogButtonSound();
        }
    }
}