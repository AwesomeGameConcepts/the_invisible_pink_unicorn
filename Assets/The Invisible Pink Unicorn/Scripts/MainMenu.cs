﻿using AGC;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject m_OptionMenu;
    [SerializeField] private GameObject m_MainMenu;
    [SerializeField] private GameObject m_RankingMenu;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        AudioManager.Instance.PlayMainMenuMusic();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ShowOption()
    {
        m_MainMenu.SetActive(false);
        m_OptionMenu.SetActive(true);
    }

    public void GoBackToMenu()
    {
        m_MainMenu.SetActive(true);
        m_OptionMenu.SetActive(false);
        m_RankingMenu.SetActive(false);
    }
}