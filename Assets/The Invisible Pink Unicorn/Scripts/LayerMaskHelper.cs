﻿using System.Collections.Generic;
using UnityEngine;

public static class LayerMaskHelper
{
    private static readonly Dictionary<string, int> _layerMaskCache = new Dictionary<string, int>();

    public static int Collectable => GetMask("Collectable");
    public static int Platform => GetMask("Platform");
    public static int Mob => GetMask("Mob");

    private static int GetMask(string name)
    {
        if (!_layerMaskCache.ContainsKey(name))
            _layerMaskCache[name] = LayerMask.GetMask(name);

        return _layerMaskCache[name];
    }
}