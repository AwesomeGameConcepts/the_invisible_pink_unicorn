﻿using System.Collections;
using UnityEngine;

public class DamageArea : MonoBehaviour
{
    [SerializeField] private int m_Damage;
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.GetComponent<MainCharacterController>())
        {
            collision.collider.GetComponent<Player>().TakeDamage(m_Damage);
            collision.collider.GetComponent<MainCharacterController>().TeleportToSecurePosition();
        }
    }
}