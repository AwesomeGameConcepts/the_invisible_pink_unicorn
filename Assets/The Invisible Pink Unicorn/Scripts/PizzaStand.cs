﻿using System.Collections;
using UnityEngine;

public class PizzaStand : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            collision.GetComponent<MainCharacterController>().LastCheckpoint = transform.position;
            collision.GetComponent<Player>().InFrontPizzaStand = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>()) collision.GetComponent<Player>().InFrontPizzaStand = false;
    }
}