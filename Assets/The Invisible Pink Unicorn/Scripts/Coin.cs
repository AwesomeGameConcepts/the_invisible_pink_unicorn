﻿using System.Collections;
using UnityEngine;

public class Coin : Collectable
{
    [SerializeField] private int m_Value;
    public int Value => m_Value;
}