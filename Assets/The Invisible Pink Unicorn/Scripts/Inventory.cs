using Assets.Ekioo.Framework.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private IntData m_MaxMoney;
    [SerializeField] private IntData m_MaxMedikit;
    [SerializeField] private IntData _pushUse;
    private List<Sock> m_LeftSocks = new List<Sock>();
    private List<Sock> m_RightSocks = new List<Sock>();
    private int m_LeftIndex = -1;
    private int m_RightIndex = -1;

    public int Money { get; private set; } = 0;
    public int Medikit { get; private set; } = 1;

    //public bool CanJump { get; private set; } = false;
    public bool CanJump => LeftSock?.Power == Sock.SockPower.Jump || RightSock?.Power == Sock.SockPower.Jump;

    public bool CanDoubleJump => LeftSock?.Power == Sock.SockPower.Jump && RightSock?.Power == Sock.SockPower.Jump;

    public bool CanLightPush => m_LeftIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Push : m_RightIndex != -1 ? m_RightSocks[m_RightIndex].Power == Sock.SockPower.Push : false;
    public bool CanHeavyPush => m_LeftIndex != -1 ? m_RightIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Push & m_RightSocks[m_RightIndex].Power == Sock.SockPower.Push : false : false;
    public bool CanThrowSmallStinkyBomb => m_LeftIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Stinky : m_RightIndex != -1 ? m_RightSocks[m_RightIndex].Power == Sock.SockPower.Stinky : false;
    public bool CanThrowBigStinkyBomb => m_LeftIndex != -1 ? m_RightIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Stinky & m_RightSocks[m_RightIndex].Power == Sock.SockPower.Stinky : false : false;
    public bool CanGlide => m_LeftIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Superman : m_RightIndex != -1 ? m_RightSocks[m_RightIndex].Power == Sock.SockPower.Superman : false;
    public bool CanLaserBeam => m_LeftIndex != -1 ? m_RightIndex != -1 ? m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Superman & m_RightSocks[m_RightIndex].Power == Sock.SockPower.Superman : false : false;

    public Sock LeftSock => m_LeftIndex != -1 ? m_LeftSocks[m_LeftIndex] : null;
    public Sock RightSock => m_RightIndex != -1 ? m_RightSocks[m_RightIndex] : null;

    public void Reset()
    {
        m_LeftSocks.Clear();
        m_RightSocks.Clear();
        m_LeftIndex = -1;
        m_RightIndex = -1;
        Money = 0;
        Medikit = 1;
        /*CanJump = false;
        CanDoubleJump = false;
        CanLightPush = false;
        CanThrowSmallStinkyBomb = false;
        CanThrowBigStinkyBomb = false;
        CanGlide = false;
        CanLaserBeam = false;*/
    }

    public void Start()
    {
        GameCanvas.Instance.HUD.SetMax(m_MaxMoney, m_MaxMedikit);
        m_LeftSocks = new List<Sock>();
        m_RightSocks = new List<Sock>();
    }

    public bool NextRightSock()
    {
        if (m_RightSocks.Count == 0) return false;
        else
        {
            /*
            //on enl�ve la chaussete de gauche
            if (m_LeftIndex != -1)
            {
                //il a une chaussette a droite
                if (m_RightIndex != -1)
                {
                    switch (m_RightSocks[m_RightIndex].Power)
                    {
                        case Sock.SockPower.Jump:
                            CanDoubleJump = false;
                            break;

                        case Sock.SockPower.Push:
                            CanHeavyPush = false;
                            break;

                        case Sock.SockPower.Stinky:
                            CanThrowBigStinkyBomb = false;
                            break;

                        case Sock.SockPower.Superman:
                            CanLaserBeam = false;
                            break;
                    }
                }
                else
                {
                    switch (m_LeftSocks[m_LeftIndex].Power)
                    {
                        case Sock.SockPower.Jump:
                            CanJump = false;
                            break;

                        case Sock.SockPower.Push:
                            CanLightPush = false;
                            break;

                        case Sock.SockPower.Stinky:
                            CanThrowSmallStinkyBomb = false;
                            break;

                        case Sock.SockPower.Superman:
                            CanGlide = false;
                            break;
                    }
                }
            }
            //on equipe la chaussette de gauche*/
            if (RightSock?.Power == Sock.SockPower.Push) _pushUse.Value--;
            m_RightIndex = (m_RightIndex + 1) % m_RightSocks.Count;
            if(m_RightSocks[m_RightIndex].Power == Sock.SockPower.Push) _pushUse.Value++;
            /*if (m_LeftIndex != -1)
            {
                switch (m_RightSocks[m_RightIndex].Power)
                {
                    case Sock.SockPower.Jump:
                        CanDoubleJump = true;
                        break;

                    case Sock.SockPower.Push:
                        CanHeavyPush = true;
                        break;

                    case Sock.SockPower.Stinky:
                        CanThrowBigStinkyBomb = true;
                        break;

                    case Sock.SockPower.Superman:
                        CanLaserBeam = true;
                        break;
                }
            }
            else
            {
                switch (m_RightSocks[m_RightIndex].Power)
                {
                    case Sock.SockPower.Jump:
                        CanJump = true;
                        break;

                    case Sock.SockPower.Push:
                        CanLightPush = true;
                        break;

                    case Sock.SockPower.Stinky:
                        CanThrowSmallStinkyBomb = true;
                        break;

                    case Sock.SockPower.Superman:
                        CanGlide = true;
                        break;
                }
            }*/
            return true;
        }
    }

    public bool NextLeftSock()
    {
        if (m_LeftSocks.Count == 0) return false;
        else
        {
            /*
            //on enl�ve la chaussete de gauche
            if (m_LeftIndex != -1)
            {
                if (m_RightIndex != -1)
                {
                    switch (m_LeftSocks[m_LeftIndex].Power)
                    {
                        case Sock.SockPower.Jump:
                            CanDoubleJump = false;
                            break;

                        case Sock.SockPower.Push:
                            CanHeavyPush = false;
                            break;

                        case Sock.SockPower.Stinky:
                            CanThrowBigStinkyBomb = false;
                            break;

                        case Sock.SockPower.Superman:
                            CanLaserBeam = false;
                            break;
                    }
                }
                else
                {
                    switch (m_LeftSocks[m_LeftIndex].Power)
                    {
                        case Sock.SockPower.Jump:
                            CanJump = false;
                            break;

                        case Sock.SockPower.Push:
                            CanLightPush = false;
                            break;

                        case Sock.SockPower.Stinky:
                            CanThrowSmallStinkyBomb = false;
                            break;

                        case Sock.SockPower.Superman:
                            CanGlide = false;
                            break;
                    }
                }
            }
            //on equipe la chaussette de gauche */
            if (LeftSock?.Power == Sock.SockPower.Push) _pushUse.Value--;
            m_LeftIndex = (m_LeftIndex + 1) % m_LeftSocks.Count;
            if (m_LeftSocks[m_LeftIndex].Power == Sock.SockPower.Push) _pushUse.Value++;
            /*if (m_RightIndex != -1)
            {
                switch (m_LeftSocks[m_LeftIndex].Power)
                {
                    case Sock.SockPower.Jump:
                        CanDoubleJump = true;
                        break;

                    case Sock.SockPower.Push:
                        CanHeavyPush = true;
                        break;

                    case Sock.SockPower.Stinky:
                        CanThrowBigStinkyBomb = true;
                        break;

                    case Sock.SockPower.Superman:
                        CanLaserBeam = true;
                        break;
                }
            }
            else
            {
                switch (m_LeftSocks[m_LeftIndex].Power)
                {
                    case Sock.SockPower.Jump:
                        CanJump = true;
                        break;

                    case Sock.SockPower.Push:
                        CanLightPush = true;
                        break;

                    case Sock.SockPower.Stinky:
                        CanThrowSmallStinkyBomb = true;
                        break;

                    case Sock.SockPower.Superman:
                         CanGlide = true;
                        break;
                }
            }*/
            return true;
        }
    }

    public void CollectSock(Sock s)
    {
        string knot="";
        string number="";
        if (s.IsLeft)
        {
            m_LeftSocks.Add(s);
            number = HaveRightSockPower(s.Power) ? "2" : "1";
        }
        else
        {
            m_RightSocks.Add(s);
            number = HaveLeftSockPower(s.Power) ? "2" : "1";
        }
        s.Collect();
        switch (s.Power)
        {
            case Sock.SockPower.Jump:
                knot = "ChaussetteKangourou";
                break;

            case Sock.SockPower.Push:
                knot = "ChaussetteHercule";
                break;

            case Sock.SockPower.Stinky:
                knot = "ChaussettePutois";
                break;

            case Sock.SockPower.Superman:
                knot = "ChaussetteSuper";
                break;
        }
        GameCanvas.Instance.DialogWindow.SetInkKnot(knot + number);
    }

    public bool HaveLeftSockPower(Sock.SockPower power)
    {
        foreach (Sock sock in m_LeftSocks)
        {
            if (sock.Power == power) return true;
        }
        return false;
    }

    public bool HaveRightSockPower(Sock.SockPower power)
    {
        foreach (Sock sock in m_RightSocks)
        {
            if (sock.Power == power) return true;
        }
        return false;
    }

    public bool AddMoney(int value)
    {
        if (Money == m_MaxMoney) return false;
        if (Money + value > m_MaxMoney) Money = m_MaxMoney;
        else Money += value;
        return true;
    }

    public bool RemoveMoney(int value)
    {
        if (Money - value < 0) return false;
        else
        {
            Money -= value;
            return true;
        }
    }

    public bool AddMedikit()
    {
        if (Medikit == m_MaxMedikit) return false;
        else
        {
            Medikit++;
            return true;
        }
    }

    public bool UseMedikit()
    {
        if (Medikit == 0) return false;
        else
        {
            Medikit--;
            return true;
        }
    }
}