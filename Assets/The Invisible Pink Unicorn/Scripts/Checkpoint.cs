﻿using System.Collections;
using UnityEngine;
public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<MainCharacterController>())
        {
            collision.GetComponent<MainCharacterController>().LastCheckpoint = transform.position;
        }
    }
}