﻿using System.Collections;
using UnityEngine;
public class Projectile : MonoBehaviour
{
    [SerializeField] private Rigidbody2D m_RigidBody;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Throw(Vector2 direction, Vector2 intialPos, float force)
    {
        m_RigidBody.velocity += direction * force;
    }
}