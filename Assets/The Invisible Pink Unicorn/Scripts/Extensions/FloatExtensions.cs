﻿using UnityEngine;
using System.Collections;

public static class FloatExtensions
{

    //return angle between 0:180 and 0:-180 exclude
    public static float AngleConvert(this float angle)
    {
        if (angle > 180) angle -= 360;
        else if (angle <= -180) angle += 360;
        return angle;
    }


    // Clamp Angle between min and max (0:180 and 0:-180 exclude)
    public static float ClampRotationAngle(this float angle, float min, float max)
    {
        angle = angle.AngleConvert();
        if (min > max)
        {
            if (angle > 0) angle = Mathf.Clamp(angle, min, 180);
            else if (angle < 0) angle = Mathf.Clamp(angle, -180, max);
        }
        else angle = Mathf.Clamp(angle, min, max);
        return angle;
    }

}
