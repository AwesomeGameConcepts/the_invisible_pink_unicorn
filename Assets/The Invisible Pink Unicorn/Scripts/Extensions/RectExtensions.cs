﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

public static class RectExtensions
{
    internal class Node
    {
        public Node rightNode;
        public Node bottomNode;
        public float pos_x;
        public float pos_z;
        public float width;
        public float length;
        public bool isOccupied;
    }

    public static List<Rect> BinPacking(this Rect container, ICollection<Rect> boxes)
    {
        Node rootNode = new Node { length = container.height, width = container.width };
        List<Rect> TmpList = boxes.OrderByDescending(x => x.width * x.height).ToList();
        Dictionary<Node, Rect> Dic = new Dictionary<Node, Rect>();
        Pack(rootNode, TmpList,Dic);
        TmpList.Clear();
        List<Rect> Result = new List<Rect>();
        foreach (KeyValuePair<Node,Rect> Pair in Dic)
        {
            Result.Add(new Rect(Pair.Key.pos_x,Pair.Key.pos_z,Pair.Value.width, Pair.Value.height));
        }
        return Result;
    }

    private static void Pack(Node rootNode, ICollection<Rect> boxes, Dictionary<Node, Rect> dic)
    {
        foreach (var box in boxes)
        {
            var node = FindNode(rootNode, box.width, box.height);
            if (node != null)
            {
                // Split rectangles
                dic.Add(SplitNode(node, box.width, box.height), box);
                //box.position = SplitNode(node, box.width, box.height);
            }
        }
    }

    private static Node FindNode(Node rootNode, float boxWidth, float boxLength)
    {
        if (rootNode.isOccupied)
        {
            var nextNode = FindNode(rootNode.bottomNode, boxWidth, boxLength);

            if (nextNode == null)
            {
                nextNode = FindNode(rootNode.rightNode, boxWidth, boxLength);
            }

            return nextNode;
        }
        else if (boxWidth <= rootNode.width && boxLength <= rootNode.length)
        {
            return rootNode;
        }
        else
        {
            return null;
        }
    }

    private static Node SplitNode(Node node, float boxWidth, float boxLength)
    {
        node.isOccupied = true;
        node.bottomNode = new Node { pos_z = node.pos_z, pos_x = node.pos_x + boxWidth, length = node.length, width = node.width - boxWidth };
        node.rightNode = new Node { pos_z = node.pos_z + boxLength, pos_x = node.pos_x, length = node.length - boxLength, width = boxWidth };
        return node;
    }
}
