﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class IEnumerableExtension
{
    public static T Random<T>(this IEnumerable<T> list)
    {
        if (!list.Any())
            return default;

        return list.ElementAt(UnityEngine.Random.Range(0, list.Count()));
    }

    public static T Random<T>(this IEnumerable<T> list, Func<T, int> getProbability, int count)
    {
        if (!list.Any())
            return default;

        int rnd = UnityEngine.Random.Range(0, count);
        foreach (var elem in list)
        {
            int probability = getProbability(elem);
            rnd -= probability;
            if (rnd <= 0)
                return elem;
        }

        throw new NotSupportedException();
    }

    public static T Random<T>(this IEnumerable<T> list, Func<T, float> getProbability, float count)
    {
        if (!list.Any())
            return default;

        float rnd = UnityEngine.Random.Range(0, count);
        foreach (var elem in list)
        {
            float probability = getProbability(elem);
            rnd -= probability;
            if (rnd <= 0)
                return elem;
        }

        throw new NotSupportedException();
    }
}