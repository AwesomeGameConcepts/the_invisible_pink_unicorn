﻿
using UnityEngine;

public static class VectorExtensions
{
    public static Vector2 Vector2 (this Vector3 Vector)
    {
        return new Vector2(Vector.x,Vector.y);
    }
    public static Vector2 Clamp(this Vector2 V, float minx, float maxx, float miny, float maxy)
    {
        return new Vector2(Mathf.Clamp(V.x, minx, maxx), Mathf.Clamp(V.y, miny, maxy));
    }
    public static Vector3 Multiply(this Vector3 Vector, Vector3 Vector3)
    {
        return new Vector3(Vector.x* Vector3.x, Vector.y * Vector3.y, Vector.z * Vector3.z);
    }
    public static Vector3 Divide(this Vector3 Vector, Vector3 Vector2)
    {
        return new Vector3(Vector.x / Vector2.x, Vector.y / Vector2.y, Vector.z / Vector2.z);
    }

    public static Vector3 Clamp(this Vector3 V, float minx, float maxx, float miny, float maxy, float minz, float maxz)
    {
        return new Vector3(Mathf.Clamp(V.x, minx, maxx), Mathf.Clamp(V.y, miny, maxy), Mathf.Clamp(V.z, minz, maxz));
    }

    public static Vector3 ClampYZ(this Vector3 V, float miny, float maxy, float minz, float maxz)
    {
        return new Vector3(V.x, Mathf.Clamp(V.y, miny, maxy), Mathf.Clamp(V.z, minz, maxz));
    }
}