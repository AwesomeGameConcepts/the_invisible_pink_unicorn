﻿using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    public static Bounds GetRendererBounds(this GameObject gameObject)
    {
        var bounds = new Bounds();
        var firstBounds = true;
        foreach (var r in gameObject.GetComponentsInChildren<Renderer>())
        {
            if (firstBounds)
            {
                bounds = r.bounds;
                firstBounds = false;
            }
            bounds.Encapsulate(r.bounds);
        }
        return bounds;
    }


    public static T AddComponent<T>(this GameObject go, T toAdd) where T : MonoBehaviour
    {
        return go.AddComponent<T>().GetCopyOf(toAdd);
    }

    // <summary>
    /// Checks a GameObject for SkinnedMeshRenderer & MeshRenderer components
    /// and returns all materials associated with either.
    /// </summary>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static List<Material> GetMaterials(this GameObject gameObject)
    {
        //null checks
        if (gameObject == null)
        {
            return null;
        }

        List<Material> mats = new List<Material>();

        foreach (Renderer ren in gameObject.GetComponents<Renderer>())
            mats.AddRange(ren.sharedMaterials);

        return mats;
    }

    /// <summary>
    /// Will return the mesh if found
    /// </summary>
    /// <param name="go">the gameobject that contains the mesh</param>
    /// <returns>the mesh found if any</returns>
    public static Mesh GetMesh(this GameObject go)
    {
        //null checks
        if (go == null)
        {
            return null;
        }
        var mf = go.GetComponent<MeshFilter>();
        var smr = go.GetComponent<SkinnedMeshRenderer>();
        var mr = go.GetComponent<MeshRenderer>();

        //priority order: advs component > vertexstream mesh renderer > mesh filter > skin mesh renderer
        //even if normally having an additionalVertexStreams means that the component advs is on the object, double check it
        if (mr != null && mr.additionalVertexStreams != null)
        {
            return mr.additionalVertexStreams;
        }
        else if (mf != null && mf.sharedMesh != null)
        {
            return mf.sharedMesh;
        }
        else if (smr != null && smr.sharedMesh != null)
        {
            return smr.sharedMesh;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Enable or disable static flag for the GameObject and all of his childrens 
    /// </summary>
    /// <param name="go"></param>
    /// <param name="setstatic">enable or disable static</param>
    public static void SetStatic(this GameObject go, bool setstatic)
    {
        for (int i = 0; i < go.transform.childCount; i++)
        {
            go.transform.GetChild(i).gameObject.isStatic = setstatic;
        }
        go.isStatic = setstatic;
    }

    public static Mesh[] GetAllMeshes(this GameObject go, bool includeinactive)
    {
        List<Transform> Transforms = new List<Transform>();
        List<Mesh> Meshes = new List<Mesh>();
        go.GetComponentsInChildren(includeinactive, Transforms);
        Mesh mesh;
        for (int i = 0; i < Transforms.Count; i++)
        {
            mesh = Transforms[i].GetMesh();
            if (mesh != null) Meshes.Add(mesh);
        }
        return Meshes.ToArray();
    }
    /// Works only if the Gameobject and it's children don't have static bacthing flag enable.
    /// </summary>
    /// <param name="go">the root game object</param>
    /// <param name="atlas">The texture for the atlas</param>
    /// <param name="material">The material to apply atlas and assign it to gameobjects</param>
    /// <param name="includeinactive">should include inactive child?</param>
    /// <param name="assignMaterial">should assign material?</param>
    /// <param name="padding">the padding of the atlas</param>
    /// <param name="size">The maxe size of atlas texture</param>
    /// <param name="makeNoLongerReadable">Should texture be no readable</param>
    /// <returns></returns>
    public static Transform[] CreateAlbedoTextureAtlas(this GameObject go, Texture2D atlas, Material material, bool includeInactive = true, int padding = 0, int size = 0, bool assignMaterial = true, bool makeNoLongerReadable = true)
    {
        Rect[] UV;
        Dictionary<Texture2D, List<Mesh>> AtlasData = new Dictionary<Texture2D, List<Mesh>>();
        List<Transform> Transforms = new List<Transform>(go.GetComponentsInChildren<Transform>(includeInactive));
        List<Material> Materials;
        Renderer[] Renderers;
        List<Texture2D> Textures;
        List<Mesh> Meshes;
        List<Mesh> RemapedMeshes = new List<Mesh>();
        Mesh mesh;
        bool found;

        //suppression des transforms sans rendu
        Transforms.GetRenderedTransform();

        for (int i = 0; i < Transforms.Count; i++)
        {
            mesh = Transforms[i].GetMesh();

            Materials = Transforms[i].GetMaterials();
            Materials.RemoveAll((Material m) => m == null);
            Textures = new List<Texture2D>();
            for (int j = 0; j < Materials.Count; j++)
            {
                if (Materials[j].mainTexture != null) Textures.Add(Materials[j].mainTexture as Texture2D);
            }
            for (int j = 0; j < Textures.Count; j++)
            {
                found = false;
                foreach (KeyValuePair<Texture2D, List<Mesh>> Pair in AtlasData)
                {
                    if (Pair.Key == Textures[j])
                    {
                        Pair.Value.Add(mesh);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    Meshes = new List<Mesh>();
                    Meshes.Add(mesh);
                    AtlasData.Add(Textures[j], Meshes);
                }
            }
        }
        Textures = new List<Texture2D>(AtlasData.Keys);
        foreach (KeyValuePair<Texture2D, List<Mesh>> Pair in AtlasData)
        {
            string tmp = "";
            foreach (Mesh m in Pair.Value)
            {
                tmp += m.name + " ";
            }
            Debug.Log(Pair.Key.name + " used in " + tmp);
        }
        //package des textures
        if (size != 0) UV = atlas.PackTextures(Textures.ToArray(), padding, size, makeNoLongerReadable);
        else UV = atlas.PackTextures(Textures.ToArray(), padding);
        foreach (Rect pos in UV)
        {
            Debug.Log(pos);
        }
        //changement des uv
        for (int i = 0; i < UV.Length; i++)
        {
            if (AtlasData.TryGetValue(Textures[i], out Meshes))
            {
                Debug.Log(Textures[i].name + " " + UV[i]);
                for (int j = 0; j < Meshes.Count; j++)
                {
                    if (!RemapedMeshes.Contains(Meshes[j]))
                    {
                        Debug.Log("UV before x " + Meshes[j].uv[0].x + " y " + Meshes[j].uv[0].y);
                        Meshes[j].RemapUV(UV[i]);
                        Debug.Log("UV after x " + Meshes[j].uv[0].x + " y " + Meshes[j].uv[0].y);
                        RemapedMeshes.Add(Meshes[j]);
                    }
                }
            }
        }

        //assignation du materiel
        if (assignMaterial)
        {
            material.SetTexture(atlas.name, atlas);
            for (int i = 0; i < Transforms.Count; i++)
            {
                Renderers = Transforms[i].GetComponents<Renderer>();
                for (int j = 0; j < Renderers.Length; j++)
                {
                    Renderers[j].material = material;
                }
            }
        }
        return Transforms.ToArray();
    }

    public static Transform[] CreateAlbedoAndNormalTextureAtlas(this GameObject go, Texture2D ablbedoatlas, Texture2D normalatlas, Material material, bool includeinactive = true, int padding = 0, int size = 0, bool makeNoLongerReadable = false)
    {
        Rect[] UV;
        Dictionary<Texture2D, List<Mesh>> AtlasData = new Dictionary<Texture2D, List<Mesh>>();
        List<Transform> Transforms = new List<Transform>(go.GetComponentsInChildren<Transform>(includeinactive));
        List<Material> Materials;
        List<Texture2D> Textures;
        List<Mesh> Meshes;
        Mesh mesh;
        bool found;
        Transforms.GetRenderedTransform();

        for (int i = 0; i < Transforms.Count; i++)
        {
            mesh = Transforms[i].GetMesh();
            Materials = Transforms[i].GetMaterials();
            Materials.RemoveAll((Material m) => m == null);
            Textures = new List<Texture2D>();
            for (int j = 0; j < Materials.Count; j++)
            {
                if (Materials[j].mainTexture != null) Textures.Add(Materials[j].mainTexture as Texture2D);
            }
            for (int j = 0; j < Textures.Count; j++)
            {
                found = false;
                foreach (KeyValuePair<Texture2D, List<Mesh>> Pair in AtlasData)
                {
                    if (Pair.Key == Textures[j])
                    {
                        Pair.Value.Add(mesh);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    Meshes = new List<Mesh>();
                    Meshes.Add(mesh);
                    AtlasData.Add(Textures[j], Meshes);
                }
            }
        }

        return Transforms.ToArray();
    }

    public static void StaticBathing(this GameObject root, Transform[] transforms)
    {
        root.SetStatic(true);
        GameObject[] gos = new GameObject[transforms.Length];
        for (int i = 0; i < transforms.Length; i++)
        {
            gos[i] = transforms[i].gameObject;
        }
        StaticBatchingUtility.Combine(gos, root);
    }

    public static void StaticBathing(this GameObject root, GameObject[] gos)
    {
        root.SetStatic(true);
        StaticBatchingUtility.Combine(gos, root);
    }
}
