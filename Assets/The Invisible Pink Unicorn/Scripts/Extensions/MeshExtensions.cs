﻿using UnityEngine;
using System.Collections.Generic;
using AGC;

public static class MeshExtensions
{
    public static void RemapUV(this Mesh mesh, Rect atlasRect)
    {
        Vector2[] NewUV = new Vector2[mesh.uv.Length];
        for (int k = 0; k < mesh.uv.Length; k++)
        {
            NewUV[k] = new Vector2(mesh.uv[k].x * atlasRect.width + atlasRect.x, mesh.uv[k].y * atlasRect.height + atlasRect.y);
        }
        mesh.uv = NewUV;
    }

    /// <summary>
    /// Copy "src" mesh values to "dest"
    /// </summary>
    /// <param name="dest">destination</param>
    /// <param name="src">source</param>
    public static void Copy(this Mesh src,Mesh dest)
    {
        //null checks
        if (dest == null || src == null)
        {
            return;
        }

        dest.Clear();
        dest.vertices = src.vertices;

        List<Vector4> uvs = new List<Vector4>();

        src.GetUVs(0, uvs); dest.SetUVs(0, uvs);
        src.GetUVs(1, uvs); dest.SetUVs(1, uvs);
        src.GetUVs(2, uvs); dest.SetUVs(2, uvs);
        src.GetUVs(3, uvs); dest.SetUVs(3, uvs);

        dest.normals = src.normals;
        dest.tangents = src.tangents;
        dest.boneWeights = src.boneWeights;
        dest.colors = src.colors;
        dest.colors32 = src.colors32;
        dest.bindposes = src.bindposes;

        dest.subMeshCount = src.subMeshCount;

        for (int i = 0; i < src.subMeshCount; i++)
            dest.SetIndices(src.GetIndices(i), src.GetTopology(i), i);

        //dest.name = src.name.IncrementPrefix("z");
    }

}
