﻿using UnityEngine;
using System.Threading.Tasks;

namespace AGC
{
    public static class TransformExtensions
    {

        //set position and rotate to align view;
        public static void SetPositionAndView(this Transform source, Transform destination, Vector3 view)
        {
            source.position = destination.position;
            source.forward = view;
        }


        //convert input in 3D Free Move
        public static Vector3 Move3DVector(this Transform transform, Vector2 input, float horizontalangle)
        {
            horizontalangle = horizontalangle.AngleConvert();
            return transform.forward * input.y * Mathf.Cos(Mathf.Deg2Rad * horizontalangle) +
                    transform.right * input.x + transform.up * input.y * Mathf.Sin(Mathf.Deg2Rad * horizontalangle);
        }

        /// <summary>
        /// Destroy all children of a transform
        /// </summary>
        /// <param name="transform">The parent to destroy all child</param>
        public static void DestroyAllChildren(this Transform transform)
        {
            while (transform.childCount > 0)
            {
                Object.DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }

        /// <summary>
        /// Load a T object from an assetbundle and set a copy as child of the transform
        /// </summary>
        /// <typeparam name="T"> the type of the object</typeparam>
        /// <param name="transform">the parent transform</param>
        /// <param name="assetBundleName">name of the assetbundle</param>
        /// <param name="objectNameToLoad">name of the object to load</param>
        /// <returns></returns>
        public static async Task<T> InstantiateFromAssetBundleAsync<T>(this Transform transform, string assetBundleName, string objectNameToLoad, AssetBundleUtility.AssetLocation location, bool unloadassetbundle) where T : UnityEngine.Object
        {
            return Object.Instantiate<T>(await AssetBundleUtility.LoadAssetAsync<T>(assetBundleName, objectNameToLoad, location, unloadassetbundle), transform);
        }

        /// <summary>
        /// Instatiate a gameObject in a transform, and move it for it's render center match origin of the parent.
        /// </summary>
        /// <param name="transform">The parent to instantiate into.</param>
        /// <param name="go">the game object to instantiate at his render center.</param>
        /// <returns>the intantiated GameObject</returns>
        public static GameObject InstatiateAtRenderCenter(this Transform transform, GameObject go)
        {
            GameObject copy = Object.Instantiate(go, transform);
            copy.transform.localPosition -= go.GetRendererBounds().center;
            return copy;
        }

        /// <summary>
        /// Return the child transform with the probided name. Search recursively
        /// </summary>
        /// <param name="trans">the parent</param>
        /// <param name="name">the child name</param>
        /// <returns></returns>
        public static Transform FindChildRecursivelyByName(this Transform trans, string name)
        {
            Transform ReturnObj;

            // If the name match, we're return it
            if (trans.name == name)
                return trans.transform;

            // Else, we go continue the search horizontaly and verticaly
            foreach (Transform child in trans)
            {
                ReturnObj = child.FindChildRecursivelyByName(name);

                if (ReturnObj != null)
                    return ReturnObj;
            }

            return null;
        }

    }
}