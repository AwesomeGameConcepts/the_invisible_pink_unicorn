﻿using UnityEngine;
using System.Collections.Generic;

public static class ComponentExtensions
{

    /// <summary>
    /// Will return the mesh if found
    /// </summary>
    /// <param name="comp">the component that contains the mesh</param>
    /// <returns>the mesh found if any</returns>
    public static Mesh GetMesh(this Component comp)
    {
        //null checks
        if (comp == null)
        {
            return null;
        }

        var mf = comp.GetComponent<MeshFilter>();
        var smr = comp.GetComponent<SkinnedMeshRenderer>();
        var mr = comp.GetComponent<MeshRenderer>();

        //priority order: advs component > vertexstream mesh renderer > mesh filter > skin mesh renderer
        //even if normally having an additionalVertexStreams means that the component advs is on the object, double check it
        if (mr != null && mr.additionalVertexStreams != null)
        {
            return mr.additionalVertexStreams;
        }
        else if (mf != null && mf.sharedMesh != null)
        {
            return mf.sharedMesh;
        }
        else if (smr != null && smr.sharedMesh != null)
        {
            return smr.sharedMesh;
        }
        else
        {
            return null;
        }
    }
    // <summary>
    /// Checks a component for SkinnedMeshRenderer & MeshRenderer components
    /// and returns all materials associated with either.
    /// </summary>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public static List<Material> GetMaterials(this Component comp)
    {
        //null checks
        if (comp == null)
        {
            return null;
        }

        List<Material> mats = new List<Material>();

        foreach (Renderer ren in comp.GetComponents<Renderer>())
            mats.AddRange(ren.sharedMaterials);

        return mats;
    }

}
