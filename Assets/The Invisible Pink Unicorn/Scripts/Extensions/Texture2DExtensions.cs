﻿using UnityEngine;
using System.IO;
using AGC;

public static class Texture2DExtensions
{
    public static void SaveToFile(this Texture2D texture, string filepath)
    {
        var tex = new Texture2D(texture.width, texture.height); 
        tex.SetPixels32(texture.GetPixels32()); 
        tex.Apply(false); 
        if (File.Exists(filepath)) File.Delete(filepath); 
        File.WriteAllBytes(filepath, tex.EncodeToPNG());
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            string filename = filePath.Split('/').Last();
            tex.name = filename.Replace("." + filename.Split('.').Last(),"");
        }
        return tex;
    }
}