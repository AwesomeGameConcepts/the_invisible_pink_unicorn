﻿using UnityEngine;
using System.Reflection;
using System;

public static class MonoBehaviourExtensions
{
    public static T GetCopyOf<T>(this MonoBehaviour Monobehaviour, T Source) where T : MonoBehaviour
    {
        //Type check
        Type type = Monobehaviour.GetType();
        if (type != Source.GetType()) return null;

        //Declare Binding Flags
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;

        //Iterate through all types until monobehaviour is reached
        while (type != typeof(MonoBehaviour))
        {
            //Apply Fields
            FieldInfo[] fields = type.GetFields(flags);
            foreach (FieldInfo field in fields)
            {
                //Debug.Log("field : " + field + " value of other : " + field.GetValue(Source));
                field.SetValue(Monobehaviour, field.GetValue(Source));
                //Debug.Log("field : " + field + " value of mono : " + field.GetValue(Monobehaviour));
            }
            //Move to base class
            type = type.BaseType;
        }
        return Monobehaviour as T;
    }

    public static T GetComponentInChildren<T>(this MonoBehaviour Monobehaviour, bool includeInactive) where T : MonoBehaviour
    {
        for (int i = 0; i < Monobehaviour.transform.childCount; i++)
        {
            if (Monobehaviour.transform.GetChild(i).GetComponentInChildren<T>(includeInactive)) return Monobehaviour.transform.GetChild(i).GetComponentInChildren<T>(includeInactive);
        }
        return null;
    }
}
