﻿using UnityEngine;
using System.Collections;

public static class ColliderExtensions
{
    /*public static void CrouchCollider(this Collider col, bool crouch, float amount)
    {
        if (col is CharacterController)
        {
            CharacterController character = (CharacterController)col;
            character.center = crouch ? character.center / amount : character.center * amount;
            character.height = crouch ? character.height / amount : character.height * amount;
        }
        else if (col is CapsuleCollider)
        {
            CapsuleCollider capsule = (CapsuleCollider)col;
            capsule.center = crouch ? capsule.center / amount : capsule.center * amount;
            capsule.height = crouch ? capsule.height / amount : capsule.height * amount;
        }
        else
        {
            Debug.LogError("Not collider type implemented for crouching. For collider named :" + col);
        }
    }*/

    public static float Volume (this CapsuleCollider caps)
    {
        return Mathf.PI * Mathf.Pow(caps.radius, 2) * (caps.height + 0.75f * caps.radius);
    }

    public static float Volume(this BoxCollider box)
    {
        return box.size.x * box.transform.lossyScale.x * box.size.y * box.transform.lossyScale.y * box.size.z * box.transform.lossyScale.z;
    }

    public static float Volume(this SphereCollider sph)
    {
        return 0.75f * Mathf.PI * Mathf.Pow(sph.radius, 3);
    }
}
