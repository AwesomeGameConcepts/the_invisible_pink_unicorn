﻿namespace AGC
{
    public static class ArrayExtensions
    {
        public static int IndexOf<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (value.Equals(array[i])) return i;
            }
            return -1;
        }

        public static bool Contain<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (value.Equals(array[i])) return true;
            }
            return false;
        }

        public static T[] AddArray<T>(this T[] array, T[] toAdd)
        {
            T[] result = new T[array.Length + toAdd.Length];
            int cpt = 0;
            foreach (T data in array)
            {
                result[cpt] = data;
                cpt++;
            }
            foreach (T data in toAdd)
            {
                result[cpt] = data;
                cpt++;
            }
            return result;
        }

        public static T[] Add<T>(this T[] array, T toAdd)
        {
            T[] result = new T[array.Length + 1];
            int cpt = 0;
            foreach (T data in array)
            {
                result[cpt] = data;
                cpt++;
            }
            result[cpt] = toAdd;
            return result;
        }

        public static T Last<T>(this T[] array)
        {
            return array[array.Length - 1];
        }

    }
}