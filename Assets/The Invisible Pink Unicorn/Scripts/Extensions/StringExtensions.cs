﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace AGC
{
    public static class StringExtensions
    {

        /// <summary>
        /// Returns a new name with incremented prefix.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string IncrementPrefix(this string name, string prefix)
        {
            string str = name;

            Regex regex = new Regex("^(" + prefix + "[0-9]*_)");
            Match match = regex.Match(name);

            if (match.Success)
            {
                string iteration = match.Value.Replace(prefix, "").Replace("_", "");
                int val = 0;

                if (int.TryParse(iteration, out val))
                {
                    str = name.Replace(match.Value, prefix + (val + 1) + "_");
                }
                else
                {
                    str = prefix + "0_" + name;
                }
            }
            else
            {
                str = prefix + "0_" + name;
            }

            return str;
        }

        public static string AssetPathRelative(this string path)
        {
            return path.Substring(Application.dataPath.Length - "Assets".Length);
        }
    }
}
