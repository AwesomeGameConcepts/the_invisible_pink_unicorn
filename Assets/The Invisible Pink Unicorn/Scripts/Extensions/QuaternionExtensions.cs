﻿using UnityEngine;
using System.Threading.Tasks;
using System.Threading;
using System;

public static class QuaternionExtensions
{
    public static Quaternion ClampRotationAroundAxis(this Quaternion rotation, Vector3 axe, float min, float max)
    {
        rotation.x /= rotation.w;
        rotation.y /= rotation.w;
        rotation.z /= rotation.w;
        rotation.w = 1.0f;

        float angle;
        if (axe.Equals(Vector3.up)) angle = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.y);
        else if (axe.Equals(Vector3.right)) angle = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.x);
        else if (axe.Equals(Vector3.forward)) angle = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.z);
        else
        {
            Debug.LogError("axe is not up or right or forward");
            return rotation;
        }
        if (min > max)
        {
            if (angle > 0) angle = Mathf.Clamp(angle, min, 180);
            else if (angle < 0) angle = Mathf.Clamp(angle, -180, max);
        }
        else angle = Mathf.Clamp(angle, min, max);

        if (axe.Equals(Vector3.up)) rotation.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle);
        else if (axe.Equals(Vector3.right)) rotation.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle);
        else if (axe.Equals(Vector3.forward)) rotation.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle);

        return rotation;
    }

    public static CancellationTokenSource DelayedLerp(this Quaternion source, Quaternion target, int repetition, int duration)
    {
        CancellationTokenSource TokenSource = new CancellationTokenSource();
        CancellationToken token = TokenSource.Token;
        source.LerpAsync(target, repetition, duration, token);
        return TokenSource;
    }

    private static async void LerpAsync(this Quaternion source, Quaternion target, int repetition, int duration, CancellationToken token)
    {
        int time;
        int wait = 0;
        //Debug.Log("LerpAsync");
        for (int i = 1; i <= repetition; i++)
        {
            if (wait > 0) await Task.Delay(wait, token);
            time = DateTime.Now.Millisecond;
            source = Quaternion.Lerp(source, target, 1 / repetition * i);
            wait = time - DateTime.Now.Millisecond + duration / repetition;
        }
    }

    /// <summary>
    /// Clamp a quaternion rotation around X axe beetween min and max
    /// </summary>
    /// <param name="q">the rotation to clamp</param>
    public static Quaternion ClampRotationAroundXAxis(this Quaternion q, float min, float max)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, min, max);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
        return q;
    }

    /// <summary>
    /// Clamp a quaternion rotation around Y axe beetween min and max
    /// </summary>
    /// <param name="q">the rotation to clamp</param>
    public static Quaternion ClampRotationAroundYAxis(this Quaternion q, float min, float max)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);
        if (min > max)
        {
            if (angleY > 0) angleY = Mathf.Clamp(angleY, min, 180);
            else if (angleY < 0) angleY = Mathf.Clamp(angleY, -180, max);
        }
        else angleY = Mathf.Clamp(angleY, min, max);
        q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);
        return q;
    }
}
