﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

public static class ObjectExtensions
{
    public static bool Copy<T>(this UnityEngine.Object Obj, T Source) where T : UnityEngine.Object
    {
        //Type check
        Type type = Obj.GetType();
        if (type != Source.GetType()) return false;
        if (Source == null || Obj == null) return false;

         //Declare Binding Flags
         BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;

        //Iterate through all types until monobehaviour is reached
        while (type != typeof(UnityEngine.Object))
        {
            //Apply Fields
            FieldInfo[] fields = type.GetFields(flags);
            foreach (FieldInfo field in fields)
            {
                //Debug.Log("field : " + field + " value of other : " + field.GetValue(Source));
                field.SetValue(Obj, field.GetValue(Source));
                //Debug.Log("field : " + field + " value of mono : " + field.GetValue(Monobehaviour));
            }
            //Move to base class
            type = type.BaseType;
        }
        return true;


    }
}