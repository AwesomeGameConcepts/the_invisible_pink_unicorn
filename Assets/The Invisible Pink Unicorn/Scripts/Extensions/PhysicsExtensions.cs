﻿using UnityEngine;
using System.Collections;

public static class PhysicsExtensions
{
    public static bool PointCast(Vector3 origin, out RaycastHit col, int layerMask, QueryTriggerInteraction triger)
    {
        return Physics.Raycast(origin, Vector3.forward, out col, 0.00001f, layerMask, triger);
    }
}
