﻿using UnityEngine;
using System.Collections;

public static class ColorExtensions
{
    public static Color SetAplha(this Color color, float alpha) => new Color(color.r, color.g, color.b, alpha);
    public static Color IncreaseAplha(this Color color, float add) => new Color(color.r, color.g, color.b, color.a + add);
    public static Color DecreaseAplha(this Color color, float substract) => new Color(color.r, color.g, color.b, color.a - substract);
}
