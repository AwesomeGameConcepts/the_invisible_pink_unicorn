﻿using UnityEngine;

public static class GizmoExtensions
{

    /// <summary>
    ///     -Draw a triangle
    /// </summary>
    /// <param name="p1">world position of the first point</param>
    /// <param name="p2">world position of the second point</param>
    /// <param name="p3">world position of the third point</param>
    /// <param name="color"> Colr of the triangle</param>
    public static void DrawTriangle(Vector3 p1, Vector3 p2, Vector3 p3, Color color)
    {
        Gizmos.color = color;
        Gizmos.DrawLine(p1, p2);
        Gizmos.DrawLine(p2, p3);
        Gizmos.DrawLine(p3, p1);
    }
}
