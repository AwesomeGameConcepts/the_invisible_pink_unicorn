﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneManagerExtensions
{
    /// <summary>
    /// Change active scene by a new scene who has been loaded additively and unload unused asset
    /// </summary>
    /// <param name="oldscene">The scene to unload</param>
    /// <param name="newscene">The scene to set active</param>
    /// <returns></returns>
    public static async Task ChangeActiveScene(string oldscene, string newscene)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(newscene));
        await SceneManager.UnloadSceneAsync(oldscene);
        await Resources.UnloadUnusedAssets();
    }
}