﻿using UnityEngine;

public static class MathfExtensions
{
    /// <summary>
    /// Change the value to get close to the target by adding or substrating step
    /// </summary>
    /// <param name="value">The value to change</param>
    /// <param name="target">The value to target</param>
    /// <param name="step">The step to add or substract</param>
    /// <returns></returns>
    public static float SetCloseTo(float value, float target, float step)
    {
        if (value > target) return Mathf.Clamp(value - step, target, value);
        else if (value < target) return Mathf.Clamp(value + step, value, target);
        else return target;
    }

    public static Vector3 SetCloseTo(Vector3 value, Vector3 target, Vector3 step)
    {
        Vector3 result = Vector3.zero;
        if (value.x > target.x) result.x = Mathf.Clamp(value.x - step.x, target.x, value.x);
        else if (value.x < target.x) result.x = Mathf.Clamp(value.x + step.x, value.x, target.x);
        else result.x = target.x;

        if (value.y > target.y) result.y = Mathf.Clamp(value.y - step.y, target.y, value.y);
        else if (value.y < target.y) result.y = Mathf.Clamp(value.y + step.y, value.y, target.y);
        else result.y = target.y;

        if (value.z > target.z) result.z = Mathf.Clamp(value.z - step.z, target.z, value.z);
        else if (value.z < target.z) result.z = Mathf.Clamp(value.z + step.z, value.z, target.z);
        else result.z = target.z;
        return result;
    }

    /// <summary>
    /// Return the positive or negatice percent of the value between min and max referenced by def
    /// </summary>
    /// <param name="val">The value to get the percent</param>
    /// <param name="min">The minimal value (100%)</param>
    /// <param name="max">The maximal value (-100%)</param>
    /// <param name="def">The default value (0%)</param>
    /// <returns>-1 or 1</returns>
    public static float GetPercent(float val, float min, float max, float def)
    {
        if (val < def) return -(def - val) / (def - min);
        else if (val > def) return (def - val) / (def - max);
        else return 0;
    }

    /// <summary>
    /// Return the value of a percent range
    /// </summary>
    /// <param name="percent">The percent of range </param>
    /// <param name="min">The minimal bound</param>
    /// <param name="max">The maximal bound</param>
    /// <returns>the value of the precent range</returns>
    public static float GetValueByPercent(float percent, float min, float max) => (1 - percent) * min + max * percent;

    /// <summary>
    /// Return the percent of this value on this range
    /// </summary>
    /// <param name="value">The value to caculate the percent</param>
    /// <param name="min">The minimal bound</param>
    /// <param name="max">T maximal bound</param>
    /// <returns>the percent 0 to 1</returns>
    public static float GetPercent(float value, float min, float max) => (value-min)/(max-min);
}
