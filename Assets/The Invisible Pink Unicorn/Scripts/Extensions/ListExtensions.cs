﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    /// <summary>
    /// remove all non rendered transforms
    /// </summary>
    /// <param name="go"></param>
    /// <param name="Transforms"></param>
    /// <returns></returns>
    public static List<Transform> GetRenderedTransform(this List<Transform> Transforms)
    {
        Mesh mesh;
        for (int i = 0; i < Transforms.Count; i++)
        {
            mesh = Transforms[i].GetMesh();
            //there is a mesh
            if (mesh != null)
            {
                //Have acces to the mesh data
                if (mesh.isReadable)
                {
                    //Mesh contain no enough vertices
                    if (mesh.vertexCount < 3)
                    {
                        Debug.LogError("Mesh : " + mesh.name + " contain no enough vertices !");
                        Transforms.RemoveAt(i);
                        i--;
                    }
                }
                else
                {
                    Debug.LogError("Can't access to mesh data for mesh " + mesh.name + ". Enable ReadAndWrite on the model in the inspector");
                    Transforms.RemoveAt(i);
                    i--;
                }
            }
            else
            {
                //Debug.LogError("GameObject " + Transforms[i].name + " don't have a mesh !");
                Transforms.RemoveAt(i);
                i--;
            }
        }
        return Transforms;
    }
}