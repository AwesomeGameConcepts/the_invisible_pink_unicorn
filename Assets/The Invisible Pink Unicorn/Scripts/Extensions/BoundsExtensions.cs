﻿using UnityEngine;

public static class BoundsExtensions
{
    public static bool IsInside(this Bounds container, Bounds inside)
    {
        if (inside.size != Vector3.zero && container.size != Vector3.zero)
        {
            return inside.min.x >= container.min.x &&
                   inside.max.x <= container.max.x &&
                   inside.min.y >= container.min.y &&
                   inside.max.y <= container.max.y &&
                   inside.min.z >= container.min.z &&
                   inside.max.z <= container.max.z;
        }
        else
        {
            Debug.LogError("Can't determinate if inside, bound size = 0. container: " + container.size + " inside: " + inside.size);
            return false;
        }
    }

    public static Bounds Intersection(this Bounds A, Bounds B)
    {
        var min = new Vector3(Mathf.Max(A.min.x, B.min.x), Mathf.Max(A.min.y, B.min.y), Mathf.Max(A.min.z, B.min.z));
        var max = new Vector3(Mathf.Min(A.max.x, B.max.x), Mathf.Min(A.max.y, B.max.y), Mathf.Min(A.max.z, B.max.z));
        return new Bounds(Vector3.Lerp(min, max, 0.5f), max - min);
    }
}
