﻿using UnityEngine;
namespace AGC
{
    static class GyroExtensions
    {
        public static Quaternion AltitudeToUnity(this Gyroscope Gyro)
        {
            return new Quaternion(Gyro.attitude.x, Gyro.attitude.y, -Gyro.attitude.z, -Gyro.attitude.w);
        }
    }
}
