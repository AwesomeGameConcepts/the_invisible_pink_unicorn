﻿using System.Collections;
using UnityEngine;

public class Sock : Collectable
{
    public enum SockPower { Jump, Push, Stinky, Superman }

    [SerializeField] private string m_SockName;
    [SerializeField] private SockPower m_Power;
    [SerializeField] private bool m_Left;

    private SpriteRenderer m_SpriteRenderer;

    protected override void Awake()
    {
        base.Awake();

        m_SpriteRenderer = GetComponent<SpriteRenderer>();

        if (IsLeft)
            m_SpriteRenderer.flipX = true;
    }

    public string Name => m_SockName;
    public bool IsLeft => m_Left;
    public SockPower Power => m_Power;
    public Sprite Sprite => m_SpriteRenderer.sprite;
}