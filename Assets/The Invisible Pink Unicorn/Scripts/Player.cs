﻿using AGC;
using Assets.Ekioo.Framework.Data;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(MainCharacterController), typeof(Inventory))]
public class Player : MonoBehaviour
{
    private Inventory m_Inventory;
    private MainCharacterController m_CharacterController;
    [SerializeField] private int m_MaxLife = 4;
    [SerializeField] private int m_Life = 4;
    [SerializeField] private int m_MedikitLifeRestore = 1;
    [SerializeField] private int m_MedikitCost = 15;
    [SerializeField] private string m_PlayerName;
    [SerializeField] private int m_DieScorePenality;
    private HUD m_HUD;
    private bool m_InFrontPizzaStand;
    private int m_Score =-1;
    private int m_KilledEnemy;
    private float m_StartTime;
    public string Name => m_PlayerName;

    public float LevelTime => Time.time - m_StartTime;

    public bool InFrontPizzaStand { get => m_InFrontPizzaStand; set
        {
            m_InFrontPizzaStand = value;
        } }

    public int Score { get => m_Score; private set
        {
            if(value != m_Score)
            {
                if (value < 0) value = 0;
                m_HUD.UdpateScore(value);
                m_Score = value;
            }
        } }

    private void Awake()
    {
        m_CharacterController = GetComponent<MainCharacterController>();
        m_Inventory = GetComponent<Inventory>();
    }


    // Use this for initialization
    private void Start()
    {
        m_HUD = GameCanvas.Instance.HUD;
        Reset();
    }

    public void Reset()
    {
        ResetLife();
        m_Inventory.Reset();
        Score = 0;
        m_StartTime = Time.time;
        m_KilledEnemy = 0;
        m_HUD.UpdateMoney(m_Inventory.Money);
        m_HUD.UpdateMedikit(m_Inventory.Medikit);
        m_HUD.UpdateRightSock(null);
        m_HUD.UpdateLeftSock(null);
    }

    public void BuyPizza()
    {
        if(m_Inventory.Money >= m_MedikitCost)
        {
            if (m_Inventory.AddMedikit())
            {
                m_Inventory.RemoveMoney(m_MedikitCost);
                m_HUD.UpdateMoney(m_Inventory.Money);
                m_HUD.UpdateMedikit(m_Inventory.Medikit);
                AudioManager.Instance.PlayBuySound();
            }
            else
            {
                //pas de place
            }
        }
        else
        {
            //pas assez d'argent
        }
    }

    public void KillEnemy(Enemy enemy)
    {
        Score = m_Score + enemy.KillScore;
        m_KilledEnemy++;
    }

    #region ChangeSocks

    public void ChangeSock1(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) EquipNextLeftSock();
    }

    public void ResetLife()
    {
        m_Life = m_MaxLife;
        m_HUD.UpdateLife(m_Life);
    }

    public void TakeDamage(int damage)
    {
        m_Life -= damage;
        if (m_Life <= 0) Die();
        else m_HUD.UpdateLife(m_Life);
    }

    public void ChangeSock2(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed) EquipNextRightSock();
    }

    #endregion ChangeSocks

    public void EquipNextLeftSock()
    {
        if (m_Inventory.NextLeftSock()) m_HUD.UpdateLeftSock(m_Inventory.LeftSock.Sprite);
    }

    public void EquipNextRightSock()
    {
        if (m_Inventory.NextRightSock()) m_HUD.UpdateRightSock(m_Inventory.RightSock.Sprite);
    }

    public void Collect(Collectable obj)
    {
        switch (obj)
        {
            case Coin coin:
                CollectCoin(coin);
                break;

            case Medikit medkit:
                CollectMedikit(medkit);
                break;

            case Sock sock:
                CollectSock(sock);
                break;
        }
        Score = m_Score + obj.ScoreValue;
    }

    public void OnMedkit(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
            UseMedikit();
    }

    public void UseMedikit()
    {
        if (m_Life < m_MaxLife)
        {
            if (m_Inventory.UseMedikit())
            {
                m_Life += m_MedikitLifeRestore;
                if (m_Life > m_MaxLife) m_Life = m_MaxLife; 
                AudioManager.Instance.PlayUseMedikitSound();
                m_HUD.UpdateLife(m_Life);
                m_HUD.UpdateMedikit(m_Inventory.Medikit);

            }
        }
    }

    private void CollectSock(Sock s)
    {
        AudioManager.Instance.PlayCollectSound();
        m_Inventory.CollectSock(s);
    }

    private void CollectCoin(Coin c)
    {
        if (m_Inventory.AddMoney(c.Value))
        {
            c.Collect();
            m_HUD.UpdateMoney(m_Inventory.Money);
            AudioManager.Instance.PlayCollectCoin();
        }
    }

    private void CollectMedikit(Medikit m)
    {
        if (m_Inventory.AddMedikit())
        {
            m.Collect();
            m_HUD.UpdateMedikit(m_Inventory.Medikit);
        }
    }

    private void Die()
    {
        m_CharacterController.IsLiving = false;
        Score = m_Score - m_DieScorePenality;
    }
}