﻿using System.Collections;
using UnityEngine;

public class HairBall : Enemy
{
    [SerializeField] private float m_WaitingChaseTime;
    protected override async void Attack(Transform playerTransform)
    {
        base.Attack(playerTransform);
        while (m_PlayerSpotedTransform != null)
        {
            m_Agent.destination = playerTransform.position;
            await new WaitForSeconds(m_WaitingChaseTime);
        }
    }
}