﻿using AGC;
using System.Collections;
using UnityEngine.AI;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;
using System;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public abstract class Enemy : MonoBehaviour
{
    [Header("Attributes")]
    [SerializeField] protected float m_MaxLife;
    [SerializeField] protected float m_ProjectionForce = 25;

    [Header("Beahviour")]
    [SerializeField] protected float m_IdleTime;
    [SerializeField] protected float m_PatrolWait;
    [SerializeField] protected GameObject m_DetectionArea;
    [SerializeField] protected UnityEvent m_OnDie;

    [Header("Movement")]
    [SerializeField] protected Transform[] m_PatrolPoint;
    [SerializeField] protected float m_Speed;
    [SerializeField] protected float m_UdpateMoveTime;
    [SerializeField] protected float m_MinDistance;

    [Header("other")]
    [SerializeField] protected int m_KillScore;
    [SerializeField] protected Collectable m_Loot;
    [SerializeField] protected SpriteRenderer m_Renderer;

    protected float m_Life;
    protected int m_PatrolIndex;
    protected Vector3 m_InitialPos;
    protected NavMeshAgent m_Agent;
    protected Rigidbody2D m_RigidBody;
    protected Transform m_PlayerSpotedTransform;
    protected CancellationTokenSource m_PatrolToken;
    protected CancellationTokenSource m_IdleToken;

    public int KillScore => m_KillScore;

    protected virtual void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_InitialPos = transform.position;
        m_Agent.updateRotation = false;
        m_Agent.updateUpAxis = false;
        Respawn(); 
    }
    //private void OnDestroy()
    //{
    //    m_PatrolToken.Cancel();
    //    m_IdleToken.Cancel();
    //}
    public void Respawn()
    {
        transform.position = m_InitialPos;
        m_IdleToken = new CancellationTokenSource();
        m_PatrolToken = new CancellationTokenSource(); 
        m_PlayerSpotedTransform = null;
        gameObject.SetActive(true);
        m_Life = m_MaxLife;
        m_PatrolIndex = 0;
        Patrol();
    }

    public void TakeDamage(float damage, Vector2 projection, Player player)
    {
        m_RigidBody.velocity += projection;
        m_Life -= damage;
        if (m_Life <= 0)
        {
            Die();
            player.KillEnemy(this);
        }
    }

    private void Die()
    {
        m_PatrolToken.Cancel();
        m_IdleToken.Cancel();
        m_OnDie.Invoke();
        if (m_Loot != null) Instantiate(m_Loot);
        gameObject.SetActive(false);
    }

    private async void Idle()
    {
        m_IdleToken = new CancellationTokenSource();
        await AsyncUtility.WaitForSecondsOrCancel(m_IdleTime, m_IdleToken.Token);
        Patrol();
    }

    private async void Patrol()
    {
        if (m_PatrolPoint.Length > 0)
        {
            m_PatrolToken = new CancellationTokenSource();
            try
            {
                while (m_PlayerSpotedTransform == null && !m_PatrolToken.IsCancellationRequested)
                {
                    Turn(m_PatrolPoint[m_PatrolIndex].position - transform.position);
                    m_Agent.destination = m_PatrolPoint[m_PatrolIndex].position;

                    Task Until = Task.Run(async () =>
                    {
                        await new WaitUntil(() => m_Life > 0 ? m_Agent.remainingDistance < m_MinDistance : false);
                    });
                    await AsyncUtility.WaitTaskOrCancel(Until, m_PatrolToken.Token);
                    if (m_PatrolToken.IsCancellationRequested) throw new OperationCanceledException();
                    await AsyncUtility.WaitForSecondsOrCancel(m_PatrolWait, m_PatrolToken.Token);
                    if (m_PatrolToken.IsCancellationRequested) throw new OperationCanceledException();
                    if (m_Agent.remainingDistance < m_MinDistance) m_PatrolIndex = (m_PatrolIndex + 1) % m_PatrolPoint.Length;
                }
            }
            catch
            {

            }
        }
        else if(m_Life > 0)
        {
            m_Agent.destination = m_InitialPos;
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            m_PlayerSpotedTransform = null;
            if(m_Life > 0) Idle();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            m_PatrolToken.Cancel();
            m_PlayerSpotedTransform = collision.transform;
            Turn(m_PlayerSpotedTransform.position - transform.position);
            Attack(m_PlayerSpotedTransform);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<MainCharacterController>())
        {
            collision.collider.GetComponent<MainCharacterController>().HitPlayer(1);
            collision.collider.attachedRigidbody.velocity += -collision.contacts[0].normal * m_ProjectionForce;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<MainCharacterController>())
        {
            collision.collider.GetComponent<MainCharacterController>().HitPlayer(1);
            collision.collider.attachedRigidbody.velocity += -collision.contacts[0].normal * m_ProjectionForce;
        }
    }

    protected virtual async void Attack(Transform playerTransform)
    {
        
    }

    private void Turn(Vector3 destination)
    {
        if (destination.x > 0)
        {
            m_Renderer.flipX = true;
            m_DetectionArea.transform.localRotation = Quaternion.Euler(m_DetectionArea.transform.localRotation.x, 180f, m_DetectionArea.transform.localRotation.z);
        }
        else
        {
            m_Renderer.flipX = false;
            m_DetectionArea.transform.localRotation = Quaternion.Euler(m_DetectionArea.transform.localRotation.x, 0, m_DetectionArea.transform.localRotation.z);
        }
    }
}