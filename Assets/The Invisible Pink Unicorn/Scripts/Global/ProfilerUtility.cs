﻿using UnityEngine;
using System.Collections;
using UnityEngine.Profiling;
using System.IO;

namespace AGC
{
    public static class ProfilerUtility
    {
        public async static void SaveProfilerData(string filepath, string fileprefix)
        {
            int count = 1;
            string path = Path.Combine(filepath, fileprefix) + count;
            // set the log file and enable the profiler
            Profiler.logFile = filepath;
            Profiler.enableBinaryLog = true;
            Profiler.enabled = true;

            // count 300 frames
            for (int i = 0; i < 300; ++i)
            {
                await new WaitForEndOfFrame();
                // workaround to keep the Profiler working
                if (!Profiler.enabled) Profiler.enabled = true;
            }
        }

    }
}
