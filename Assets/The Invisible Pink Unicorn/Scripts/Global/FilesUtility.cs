﻿
using System.Collections;
using System.IO;

namespace AGC
{
    public static class DirectoryUtility
    {
        
        public static bool CreateDirectory(string Path)
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
                return true;
            }
            else return false;
        }

    }
    public static class FileUtility
    {
        public static void InsertLineAtIndex(string filepath, int index, string text)
        {
            ArrayList lines = new ArrayList(File.ReadAllLines(filepath));
            StreamWriter wrtr = new StreamWriter(filepath);

            if (lines.Count > index) lines.Insert(index, text);
            else lines.Add(text);
            for (int i = 0; i < lines.Count; i++) wrtr.WriteLine(lines[i]);
            wrtr.Close();
        }

        public static void DeleteLineAtIndex(string filepath, int index)
        {
            ArrayList lines = new ArrayList(File.ReadAllLines(filepath));
            StreamWriter wrtr = new StreamWriter(filepath);
            for (int i = 0; i < lines.Count; i++) if(index != i) wrtr.WriteLine(lines[i]);
            wrtr.Close();
        }
    }
}