﻿using UnityEngine;
using System.Collections;

public static class PlatformUtility
{
#if UNITY_ANDROID
    public static string OS = "Android";
#elif UNITY_STANDALONE_WIN
    public static string OS = SystemInfo.operatingSystem.Contains("64") ? "StandaloneWindows64" : "StandaloneWindows";
#else
    public static string OS = SystemInfo.operatingSystem;
#endif

}
