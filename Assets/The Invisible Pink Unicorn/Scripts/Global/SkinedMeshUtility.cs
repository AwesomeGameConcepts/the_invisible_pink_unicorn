﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace AGC
{
	public static class SkinnedMeshUtility
	{

		/// <summary>
		/// Return the list of all the new skinned mesh renderer added to root.
		/// </summary>
		/// <param name="obj">root of the object containing the animation component, the bones and the skinned mesh renderer you want to add to the main object</param>
		/// <param name="parent">the transform of that main object</param>
		/// <returns>The list of all the new skinned mesh renderer added to root</returns>
		public static List<GameObject> AddSkinnedMeshTo(GameObject obj, Transform parent, Transform root)
		{
			List<GameObject> result = new List<GameObject>();
			SkinnedMeshRenderer[] BonedObjects = obj.GetComponentsInChildren<SkinnedMeshRenderer>(true);
			foreach (SkinnedMeshRenderer smr in BonedObjects)
				result.Add(ProcessBonedObject(smr, parent, root));

			return result;
		}

		public static GameObject Combine(List<SkinnedMeshRenderer> skinnedMeshRenderers, Bounds localBounds, Transform armature)
		{
			GameObject combinedSkinnedMeshGO = new GameObject("CombinedSkinnedMesh");

			if (skinnedMeshRenderers.Count == 0)
				return combinedSkinnedMeshGO;

			List<BoneWeight> boneWeights = new List<BoneWeight>();
			List<Transform> bones = new List<Transform>();
			List<CombineInstance> combineInstances = new List<CombineInstance>();
			Material sharedMaterial = skinnedMeshRenderers[0].sharedMaterial;

			int num = 0;
			for (int i = 0; i < skinnedMeshRenderers.Count; ++i)
			{
				SkinnedMeshRenderer skinnedMeshRenderer = skinnedMeshRenderers[i];
				BoneWeight[] bws = skinnedMeshRenderer.sharedMesh.boneWeights;
				Transform[] bs = skinnedMeshRenderer.bones;
				/* bug
				Transform[] bs = new Transform[skinnedMeshRenderer.bones.Length];
				// As clips are using bones by their names, we find them that way.
				for (int j = 0; j < skinnedMeshRenderer.bones.Length; i++)
					bs[j] = armature.FindChildRecursivelyByName(skinnedMeshRenderer.bones[j].name);
				*/
				for (int bwIndex = 0; bwIndex < bws.Length; ++bwIndex)
				{
					BoneWeight boneWeight = bws[bwIndex];
					boneWeight.boneIndex0 += num;
					boneWeight.boneIndex1 += num;
					boneWeight.boneIndex2 += num;
					boneWeight.boneIndex3 += num;

					//if(!boneWeights.Contains(boneWeight)) boneWeights.Add(boneWeight);
					boneWeights.Add(boneWeight);
				}
				num += bs.Length;

				for (int boneIndex = 0; boneIndex < bs.Length; ++boneIndex)
				{
					//if (!bones.Contains(bs[boneIndex])) bones.Add(bs[boneIndex]);
					bones.Add(bs[boneIndex]);
				}

				CombineInstance combineInstance = new CombineInstance()
				{
					mesh = skinnedMeshRenderer.sharedMesh,
					//transform = skinnedMeshRenderer.transform.localToWorldMatrix
					transform = skinnedMeshRenderer.transform.worldToLocalMatrix
				};
				combineInstances.Add(combineInstance);

				skinnedMeshRenderer.enabled = false;
			}

			List<Matrix4x4> bindposes = new List<Matrix4x4>();
			for (int i = 0; i < bones.Count; ++i)
			{
				Transform bone = bones[i];
				bindposes.Add(bone.worldToLocalMatrix * combinedSkinnedMeshGO.transform.worldToLocalMatrix);
			}

			SkinnedMeshRenderer combinedSkinnedMeshRenderer = combinedSkinnedMeshGO.AddComponent<SkinnedMeshRenderer>();
			combinedSkinnedMeshRenderer.updateWhenOffscreen = false;
			combinedSkinnedMeshRenderer.localBounds = localBounds;
			combinedSkinnedMeshRenderer.sharedMesh = new Mesh();
			combinedSkinnedMeshRenderer.sharedMesh.CombineMeshes(combineInstances.ToArray(), true, true);
			//combinedSkinnedMeshRenderer.sharedMesh.Optimize();
			combinedSkinnedMeshRenderer.sharedMaterial = sharedMaterial;
			//combinedSkinnedMeshRenderer.localBounds = combinedSkinnedMeshGO.GetRendererBounds();
			combinedSkinnedMeshRenderer.bones = bones.ToArray();
			Debug.Log("Bone count " + bones.Count + " weight count " + boneWeights.Count + " mesh vertices count " + combinedSkinnedMeshRenderer.sharedMesh.vertices.Length);
			combinedSkinnedMeshRenderer.sharedMesh.boneWeights = boneWeights.ToArray();
			combinedSkinnedMeshRenderer.sharedMesh.bindposes = bindposes.ToArray();
			combinedSkinnedMeshRenderer.sharedMesh.RecalculateBounds();

			return combinedSkinnedMeshGO;
		}

		private static GameObject ProcessBonedObject(SkinnedMeshRenderer ThisRenderer, Transform Parent, Transform root)
		{
			// Create the SubObject
			GameObject newObject = new GameObject(ThisRenderer.gameObject.name);
			newObject.transform.parent = Parent;

			// Add the renderer
			//SkinnedMeshRenderer NewRenderer = newObject.AddComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
			SkinnedMeshRenderer NewRenderer = newObject.AddComponent<SkinnedMeshRenderer>();

			// Assemble Bone Structure	
			Transform[] MyBones = new Transform[ThisRenderer.bones.Length];

			// As clips are using bones by their names, we find them that way.
			for (int i = 0; i < ThisRenderer.bones.Length; i++)
				MyBones[i] = Parent.FindChildRecursivelyByName(ThisRenderer.bones[i].name);

			///
			/*
			List<Matrix4x4> bindposes = new List<Matrix4x4>();
			for (int i = 0; i < MyBones.Length; ++i)
			{
				Transform bone = MyBones[i];
				bindposes.Add(bone.worldToLocalMatrix * NewRenderer.transform.worldToLocalMatrix);
			}*/

			// Assemble Renderer	
			NewRenderer.bones = MyBones;
			NewRenderer.rootBone = root;
			NewRenderer.sharedMesh = ThisRenderer.sharedMesh;
			NewRenderer.materials = ThisRenderer.materials;

			//
			//NewRenderer.sharedMesh.bindposes = bindposes.ToArray();

			return newObject;
		}
	}


	/// <summary>
	/// Use this class for avoid deformation when adding Skinned mesh when the Root is not on TPose
	/// var hierarchyOnStart = new Bone(this.transform); for exemple in Start function
	/// var hierarchyOnStitch = new Bone(this.transform); just before stitch
	/// hierarchyOnStart.Restore(); before stitch
	/// stitch function
	/// hierarchyOnStitch.Restore(); when stich function is over
	/// </summary>
	public class Bone
	{
		#region Fields
		//  I like to store as much info as I can for later use
		public string name;
		private Transform transform;
		private Vector3 localPosition;
		private Quaternion localRotation;
		private Vector3 localScale;
		private List<Bone> children = new List<Bone>();
		#endregion

		#region Constructors
		/// <summary>
		/// We use the constructor to map the current state of the bone, and it's children, recursivly
		/// </summary>
		/// <param name="transform"></param>
		public Bone(Transform transform)
		{
			this.transform = transform;
			name = transform.name;
			localPosition = transform.localPosition;
			localRotation = transform.localRotation;
			localScale = transform.localScale;
			foreach (Transform child in transform)
				children.Add(new Bone(child));
		}
		#endregion

		#region Restoration
		/// <summary>
		/// Restore all bones to mapped state
		/// </summary>
		public void Restore()
		{
			transform.localPosition = localPosition;
			transform.localRotation = localRotation;
			transform.localScale = localScale;
			foreach (var child in children)
				child.Restore();
		}
		#endregion

	}
}