﻿using UnityEngine;
using System.Threading.Tasks;
using TMPro;
using System.Threading;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace AGC
{
    //To do should loop Async with delay for instant cancel ?

    /*public class TaskManager
    {
        public CancellationTokenSource TokenSource = new CancellationTokenSource();
        private List<Task> m_Tasks = new List<Task>();

        /*public void Clear()
        {
            for (int i = 0; i < Tasks.Count; i++)
            {
                if (Tasks[i].Status == TaskStatus.RanToCompletion)
                {
                    Tasks[i].Dispose();
                    Tasks.Remove(Tasks[i]);
                    i--;
                }
            }
        }

        public Task AddTask(Task T)
        {
            m_Tasks.Add(T);
            return T;
        }

        public async Task Cancel()
        {
            TokenSource.Cancel();
            for (int i = 0; i < m_Tasks.Count; i++)
            {
                Debug.Log(m_Tasks.Count);
                while (m_Tasks[i].Status != TaskStatus.RanToCompletion)
                {
                    await new WaitForSecondsRealtime(0.1f);
                }
                m_Tasks[i].Dispose();
            }
            m_Tasks.Clear();
            TokenSource = new CancellationTokenSource();
        }
    }
    */
    public class AsyncUtility
    {
        public async static Task WaitForSecondsOrCancel(float time, CancellationToken ct)
        {
            Task cancelTask = Task.Run(async () =>
            {
                await new WaitUntil(() => ct.IsCancellationRequested);
            });
            Task Wait = Task.Run(async () =>
            {
                await new WaitForSeconds(time);
            });
            await Task.WhenAny(new[] { Wait, cancelTask });
        }

        public async static Task WaitForSecondsRealtimeOrCancel(float time, CancellationToken ct)
        {
            Task cancelTask = Task.Run(async () =>
            {
                await new WaitUntil(() => ct.IsCancellationRequested);
            });
            Task Wait = Task.Run(async () =>
            {
                await new WaitForSecondsRealtime(time);
            });
            await Task.WhenAny(new[] { Wait, cancelTask });
        }

        public async static Task WaitTaskOrCancel(Task task, CancellationToken ct)
        {
            Task cancelTask = Task.Run(async () =>
            {
                await new WaitUntil(() => ct.IsCancellationRequested);
            });
            await Task.WhenAny(new[] { task, cancelTask });
        }
    }
    [Serializable]
    public class FloatAsync
    {
        public float value;
        public CancelAction OnCancel = CancelAction.Final;

        /// <summary>
        /// Add value to current float during duration
        /// </summary>
        /// <param name="add">value to add</param>
        /// <param name="duration">total duration in seconds using time scale</param>
        /// <param name="iteration">the number of iteration</param>
        public async Task IncrementAsync(float add, float duration, int iteration)
        {
            float toadd = add / iteration;
            float durationstep = duration / iteration;
            for(int i = 0; i < iteration; i++)
            {
                value += toadd;
                await new WaitForSeconds(durationstep);
            }
        }

        /// <summary>
        /// Add value to current float during duration with start delay
        /// </summary>
        /// <param name="delay">delay before start adding</param>
        /// <param name="add">value to add</param>
        /// <param name="duration">total duration in seconds using time scale</param>
        /// <param name="iteration">the step number of iteration</param>
        public async Task DelayedIncrementAsync(float delay, float add, float duration, int iteration)
        {
            await new WaitForSeconds(delay);
            await IncrementAsync(add, duration, iteration);
        }
        /// <summary>
        /// Add value to current float during duration with start delay. Can be canceled by token.
        /// </summary>
        /// <param name="delay">delay before start adding</param>
        /// <param name="add">value to add</param>
        /// <param name="duration">total duration in seconds using time scale</param>
        /// <param name="iteration">the step number of iteration</param>
        /// <param name="ct">the cancelation token</param>
        public async Task DelayedIncrementAsync(float delay, float add, float duration, int iteration, CancellationToken ct)
        {

            await AsyncUtility.WaitForSecondsOrCancel(delay, ct);
            try
            {
                if (ct.IsCancellationRequested) throw new OperationCanceledException();
                await IncrementAsync(add, duration, iteration, ct);
            }
            catch 
            {
                if (OnCancel == CancelAction.Final) value +=add;
            }
        }


        /// <summary>
        /// Add value to current float during duration. Can be canceled by token.
        /// </summary>
        /// <param name="add">value to add</param>
        /// <param name="duration">total duration in seconds using time scale</param>
        /// <param name="iteration">the number of iteration</param>
        /// <param name="ct">the cancelation token</param>
        public async Task IncrementAsync(float add, float duration, int iteration, CancellationToken ct)
        {
            float initial = value;
            try
            {
                float toadd = add / iteration;
                float durationstep = duration / iteration;
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    value += toadd;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (OnCancel == CancelAction.Final) value = initial + add;
                else if (OnCancel == CancelAction.Initial) value = initial;
            }
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public static implicit operator FloatAsync(int v) => new FloatAsync { value = v };
        public static implicit operator FloatAsync(float v) => new FloatAsync { value = v };
        public static implicit operator float(FloatAsync af) => af.value;
        public static implicit operator int(FloatAsync af) => (int) af.value;
        public static float operator +(FloatAsync af, float f) => af.value + f;
        public static float operator -(FloatAsync af, float f) => af.value - f;
        public static float operator *(FloatAsync af, float f) => af.value * f;
        public static float operator /(FloatAsync af, float f) => af.value / f;
    }

    public static class TransformAsync
    {
        #region Async Function
        /// <summary>
        /// Translate Transform to destination.
        /// </summary>
        /// <param name="trans">The Transform to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task TranslateAsync(this Transform trans, Vector3 dest, Space space, float duration, int iteration)
        {
            Vector3 move = (dest - trans.position) / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                trans.Translate(move,space);
                await new WaitForSeconds(durationstep);
            }
        }
        /// <summary>
        /// Wait delay and translate Transform to destination during duration
        /// </summary>
        /// <param name="trans">The Transform to move</param>
        /// <param name="delay">The delay for start translate</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task DelayedTranslateAsync(this Transform trans, float delay, Vector3 dest, Space space, float duration, int iteration)
        {
            await new WaitForSeconds(delay);
            await trans.TranslateAsync(dest, space, duration, iteration);
        }

        /// <summary>
        /// Scale a transform from start to end during duration
        /// </summary>
        /// <param name="trans">The transform to scale</param>
        /// <param name="start">The start scale</param>
        /// <param name="duration">The end scale</param>
        /// <param name="duration">The duration of scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task ScaleAsync(this Transform trans, Vector3 start, Vector3 end, float duration, int iteration)
        {
            trans.localScale = start;
            Vector3 toadd = (end - start ) / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                trans.localScale += toadd;
                await new WaitForSeconds(durationstep);
            }
        }

        #endregion
        #region Async Function with Cancellation token
        /// <summary>
        /// Translate Transform to destination during duration. Can be canceled with cancellation token.
        /// </summary>
        /// <param name="trans">The Transform to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at this position. Initial : Set to intial position. Final : Set to dest position.</param>
        /// <param name="ct">the cancelation token</param>
        /// <returns></returns>
        public async static Task TranslateAsync(this Transform trans, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            Vector3 initialpos = trans.position;
            Vector3 move = (dest - initialpos) / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    trans.Translate(move);
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Initial) trans.position = initialpos;
                else if (cancelaction == CancelAction.Final) trans.position = dest;
            }
        }

        /// <summary>
        /// Wait delay and translate Transform to destination during duration. Can be canceled with cancellation token.
        /// </summary>
        /// <param name="trans">The Transform to move</param>
        /// <param name="delay">The delay for start translate</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at this position. Initial : Set to intial position. Final : Set to dest position.</param>
        /// <param name="ct">the cancelation token</param>
        /// <returns></returns>
        public async static Task DelayedTranslateAsync(this Transform trans, float delay, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            await AsyncUtility.WaitForSecondsOrCancel(delay, ct);
            try
            {
                if (ct.IsCancellationRequested) throw new OperationCanceledException();
                await trans.TranslateAsync(dest, duration, iteration, cancelaction, ct);
            }
            catch
            {
                if (cancelaction == CancelAction.Final) trans.position = dest;
            }
        }
        /*
        public static async Task MoveAlongCinemachinePath(this Transform trans, CinemachinePathBase Path, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float deltaPath = Path.MaxPos / iteration;
            float durationstep = duration / iteration;
            float Pos = 0;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Pos += deltaPath;
                    trans.position = Path.EvaluatePosition(Pos);
                    trans.rotation = Path.EvaluateOrientation(Pos);
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final)
                {
                    trans.position = Path.EvaluatePosition(Path.PathLength);
                    trans.rotation = Path.EvaluateOrientation(Path.PathLength);
                }
                else if (cancelaction == CancelAction.Initial)
                {
                    trans.position = Path.EvaluatePosition(0);
                    trans.rotation = Path.EvaluateOrientation(0);
                }
            }
        }
        */
        #endregion
    }

    public static class TextMeshProAsync
    {
        #region Async Functions
        /// <summary>
        /// Translate TextMeshPro Text to destination during duration with alpha fading
        /// </summary>
        /// <param name="TMP">The TextMeshPro to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task FadeTranslateAsync(this TextMeshProUGUI TMP, Vector3 dest, float duration, int iteration)
        {
            Vector3 move = (dest - TMP.transform.position) / iteration;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                TMP.transform.Translate(move);
                TMP.alpha -=fadestep;
                await new WaitForSeconds(durationstep);
            }
        }

        /// <summary>
        /// Fade TextMeshPro Text during duration
        /// </summary>
        /// <param name="TMP">The TextMeshPro to fade</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task FadeInAsync(this TextMeshProUGUI TMP, float duration, int iteration)
        {
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                TMP.alpha -= fadestep;
                await new WaitForSeconds(durationstep);
            }
        }

        /// <summary>
        /// Fade TextMeshPro Text during duration
        /// </summary>
        /// <param name="TMP">The TextMeshPro to fade</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task FadeOutAsync(this TextMeshProUGUI TMP, float duration, int iteration)
        {
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                TMP.alpha += fadestep;
                await new WaitForSeconds(durationstep);
            }
        }

        /// <summary>
        /// Translate parent of TextMeshProText to destination during duration with alpha fading for each child
        /// Used with GetComponentsInChildren<TextMeshProText>() 
        /// </summary>
        /// <param name="TMPComponents">The TextMeshPro to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task ChildFadeTranslateAsync(this TextMeshProUGUI[] TMPComponents, Vector3 dest, float duration, int iteration)
        {
            Transform parent = TMPComponents[0].transform.parent;
            Vector3 move = (dest - parent.position) / iteration;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                foreach (TextMeshProUGUI TMP in TMPComponents) TMP.alpha -= fadestep;
                parent.Translate(move);
                await new WaitForSeconds(durationstep);
            }
        }

        /// <summary>
        /// Wait delay and translate TextMeshPro Text to destination during duration with alpha fading
        /// </summary>
        /// <param name="TMP">The TextMeshPro to move</param>
        /// <param name="delay">The delay for start translate</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task DelayedFadeTranslateAsync(this TextMeshProUGUI TMP, float delay, Vector3 dest, float duration, int iteration)
        {
            await new WaitForSeconds(delay);
            await TMP.FadeTranslateAsync(dest, duration, iteration);
        }

        /// <summary>
        /// Wait delay and fade TextMeshPro Text during duration
        /// </summary>
        /// <param name="TMP">The TextMeshPro to fade</param>
        /// <param name="delay">The delay for start fade</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task DelayedFadeAsync(this TextMeshProUGUI TMP, float delay, float duration, int iteration)
        {
            await new WaitForSeconds(delay);
            await TMP.FadeInAsync(duration, iteration);
        }

        /// <summary>
        ///  Wait delay and translate parent of TextMeshProText to destination during duration with alpha fading for each child
        /// Used with GetComponentsInChildren<TextMeshProText>() 
        /// </summary>
        /// <param name="TMPComponents">The TextMeshPro to move</param>
        /// <param name="delay">The delay for start fade</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <returns></returns>
        public async static Task DelayedChildFadeTranslateAsync(this TextMeshProUGUI[] TMPComponents, float delay, Vector3 dest, float duration, int iteration)
        {
            await new WaitForSeconds(delay);
            await TMPComponents.ChildFadeTranslateAsync(dest, duration, iteration);
        }
        #endregion

        #region Async Function With Cancellation token
        /// <summary>
        /// Translate TextMeshPro Text to destination during duration with alpha fading. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="TMP">The TextMeshPro to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current position with current alpha value. 
        /// Initial : Set to initial position with initial alpha. Final : Set to dest position with final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task FadeTranslateAsync(this TextMeshProUGUI TMP, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            Vector3 initialpos = TMP.transform.position;
            float initialalpha = TMP.alpha;
            Vector3 move = (dest - initialpos) / iteration;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    TMP.transform.Translate(move);
                    TMP.alpha -= fadestep;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if(cancelaction == CancelAction.Final)
                {
                    TMP.transform.position = dest;
                    TMP.alpha = 0;
                }
                else if (cancelaction == CancelAction.Initial)
                {
                    TMP.transform.position = initialpos;
                    TMP.alpha = initialalpha;
                }
            }
        }

        /// <summary>
        /// Fade TextMeshPro Text during duration. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="TMP">The TextMeshPro to fade</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current alpha value. 
        /// Initial : Set to initial alpha. Final : Set to final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task FadeInAsync(this TextMeshProUGUI TMP, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float initialalpha = TMP.alpha;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    TMP.alpha -= fadestep;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final) TMP.alpha = 0;
                else if (cancelaction == CancelAction.Initial) TMP.alpha = initialalpha;
            }
        }

        /// <summary>
        /// Fade TextMeshPro Text during duration. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="TMP">The TextMeshPro to fade</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current alpha value. 
        /// Initial : Set to initial alpha. Final : Set to final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task FadeOutAsync(this TextMeshProUGUI TMP, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float initialalpha = TMP.alpha;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    TMP.alpha += fadestep;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final) TMP.alpha = 1;
                else if (cancelaction == CancelAction.Initial) TMP.alpha = initialalpha;
            }
        }

        /// <summary>
        /// Translate parent of TextMeshProText to destination during duration with alpha fading for each child. Can be cancelled by cancellation token.
        /// Used with GetComponentsInChildren<TextMeshProText>() 
        /// </summary>
        /// <param name="TMPComponents">The TextMeshPro to move</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current position with current alpha value. 
        /// Initial : Set to initial position with initial alpha. Final : Set to dest position with final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task ChildFadeTranslateAsync(this TextMeshProUGUI[] TMPComponents, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            Transform parent = TMPComponents[0].transform.parent;
            Vector3 initialpos = parent.position;
            float[] intialaplha = new float[TMPComponents.Length];
            for (int i = 0; i < TMPComponents.Length; i++) intialaplha[i] = TMPComponents[i].alpha;
            Vector3 move = (dest - initialpos) / iteration;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    foreach (TextMeshProUGUI TMP in TMPComponents) TMP.alpha -= fadestep;
                    parent.Translate(move);
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final)
                {
                    foreach (TextMeshProUGUI TMP in TMPComponents) TMP.alpha = 0;
                    parent.position = dest;
                }
                else if (cancelaction == CancelAction.Initial)
                {
                    for (int i = 0; i < TMPComponents.Length; i++) TMPComponents[i].alpha = intialaplha[i];
                    parent.position = initialpos;
                }
            }
        }

        /// <summary>
        /// Wait delay and translate TextMeshPro Text to destination during duration with alpha fading. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="TMP">The TextMeshPro to move</param>
        /// <param name="delay">The delay for start translate</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current position with current alpha value. 
        /// Initial : Set to initial position with initial alpha. Final : Set to dest position with final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task DelayedFadeTranslateAsync(this TextMeshProUGUI TMP, float delay, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            await AsyncUtility.WaitForSecondsOrCancel(delay, ct);
            try
            {
                if (ct.IsCancellationRequested) throw new OperationCanceledException();
                await TMP.FadeTranslateAsync(dest, duration, iteration, cancelaction,ct);
            }
            catch
            {
                if (cancelaction == CancelAction.Final)
                {
                    TMP.transform.position = dest;
                    TMP.alpha = 0;
                }
            }
        }

        /// <summary>
        ///  Wait delay and translate parent of TextMeshProText to destination during duration with alpha fading for each child. Can be cancelled by cancellation token.
        /// Used with GetComponentsInChildren<TextMeshProText>() 
        /// </summary>
        /// <param name="TMPComponents">The TextMeshPro to move</param>
        /// <param name="delay">The delay for start fade</param>
        /// <param name="dest">The destination in WorldSpace</param>
        /// <param name="duration">Total duration in seconds using time scale</param>
        /// <param name="iteration">The number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current position with current alpha value. 
        /// Initial : Set to initial position with initial alpha. Final : Set to dest position with final alpha value.</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task DelayedChildFadeTranslateAsync(this TextMeshProUGUI[] TMPComponents, float delay, Vector3 dest, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            await AsyncUtility.WaitForSecondsOrCancel(delay, ct);
            try
            {
                if (ct.IsCancellationRequested) throw new OperationCanceledException();
                await TMPComponents.ChildFadeTranslateAsync(dest, duration, iteration, cancelaction, ct);
            }
            catch
            {
                if (cancelaction == CancelAction.Final)
                {
                    foreach (TextMeshProUGUI TMP in TMPComponents) TMP.alpha = 0;
                    TMPComponents[0].transform.parent.position = dest;
                }
            }
        }

        #endregion
    }

    public static class ImgAsync
    {
        #region Async Functions
        /// <summary>
        /// Decrease Alpha of an Image during duration
        /// </summary>
        /// <param name="Img">the Image to fade</param>
        /// <param name="duration">the duration of fadein</param>
        /// <param name="iteration">the number of iteration</param>
        /// <returns></returns>
        public async static Task FadeInAsync(this Image Img, float duration, int iteration)
        {
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                Img.color = Img.color.DecreaseAplha(fadestep);
                await new WaitForSeconds(durationstep);
            }
        }
        /// <summary>
        /// Increase Alpha of an Image during duration
        /// </summary>
        /// <param name="Img">the Image to fade</param>
        /// <param name="duration">the duration of fadein</param>
        /// <param name="iteration">the number of iteration</param>
        /// <returns></returns>
        public async static Task FadeOutAsync(this Image Img, float duration, int iteration)
        {
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            for (int i = 0; i < iteration; i++)
            {
                Img.color = Img.color.IncreaseAplha(fadestep);
                await new WaitForSeconds(durationstep);
            }
        }
        #endregion
        #region Async Functions Witch Cancellation token
        /// <summary>
        /// Decrease Alpha of an Image during duration. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="Img">the Image to fade</param>
        /// <param name="duration">the duration of fadein</param>
        /// <param name="iteration">the number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current alpha. 
        /// Initial : Set to initial alpha. Final : Set to final alpha.</param>
        /// <param name="ct">The Cancellation Token.</param>
        /// <returns></returns>
        public async static Task FadeInAsync(this Image Img, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float initialalpha = Img.color.a;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Img.color = Img.color.DecreaseAplha(fadestep);
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final) Img.color = Img.color.SetAplha(0);
                else if (cancelaction == CancelAction.Initial) Img.color = Img.color.SetAplha(initialalpha);
            }
        }
        /// <summary>
        /// Increase Alpha of an Image during duration. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="Img">the Image to fade</param>
        /// <param name="duration">the duration of fadeout/param>
        /// <param name="iteration">the number of iteration</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current alpha. 
        /// Initial : Set to initial alpha. Final : Set to final alpha.</param>
        /// <param name="ct">The Cancellation Token.</param>
        /// <returns></returns>
        public async static Task FadeOutAsync(this Image Img, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float initialalpha = Img.color.a;
            float fadestep = 1f / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Img.color = Img.color.IncreaseAplha(fadestep);
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final) Img.color = Img.color.SetAplha(0);
                else if (cancelaction == CancelAction.Initial) Img.color = Img.color.SetAplha(initialalpha);
            }
        }
        #endregion
    }

    public static class ObjectAsync
    {
        /// <summary>
        /// Destroy an object after delay.
        /// </summary>
        /// <param name="obj">The object to destroy.</param>
        /// <param name="delay">Wait this time in seconds using time scale</param>
        /// <returns></returns>
        public async static Task DelayedDestroyAsync(this UnityEngine.Object obj, float delay)
        {
            await new WaitForSeconds(delay);
            UnityEngine.Object.Destroy(obj);
        }

        /// <summary>
        /// Destroy an object after delay. Can bel cancelled by cancellation token.
        /// </summary>
        /// <param name="obj">The object to destroy.</param>
        /// <param name="delay">Wait this time in seconds using time scale</param>
        /// <param name="ct">The cancellation token</param>
        /// <returns></returns>
        public async static Task DelayedDestroyAsync(this UnityEngine.Object obj, float delay, CancellationToken ct)
        {
            await AsyncUtility.WaitForSecondsOrCancel(delay, ct);
            if (!ct.IsCancellationRequested) UnityEngine.Object.Destroy(obj);
        }
    }
    /*
    public static class CinemachineAsync
    {
        /// <summary>
        /// Translate Camera along the dolly track on given time with fixed number of transition. Can be cancelled by cancellation token.
        /// </summary>
        /// <param name="Dolly">The dolly track to use. Can be get from CinemachineVirtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>().</param>
        /// <param name="duration">The total duration of the translation.</param>
        /// <param name="iteration">The number of iteration.</param>
        /// <param name="cancelaction">If canceled, set state. Stop : Stop at current position. 
        /// Initial : Set to initial position. Final : Set to final position.</param>
        /// <param name="ct">The Cancellation Token.</param>
        /// <returns></returns>
        public async static Task TravelingAsync(this CinemachineTrackedDolly Dolly, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float deltaPath = Dolly.m_Path.MaxPos / iteration;
            float durationstep = duration / iteration;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Dolly.m_PathPosition += deltaPath;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep, ct);
                }
            }
            catch
            {
                if (cancelaction == CancelAction.Final) Dolly.m_PathPosition = Dolly.m_Path.MaxPos;
                else if (cancelaction == CancelAction.Initial) Dolly.m_PathPosition = Dolly.m_Path.MinPos;
            }
        }

    }
    */
    public static class AudioAsync
    {
        public static async Task CurveFadeInAsync(AudioSource Source, AnimationCurve Curve, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float curvedelta = 1f / iteration;
            float durationstep = duration / iteration;
            float timestart; //this variables are used to avoid time offset
            float timedelay = 0; //
            float actualcurvevalue=0f;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    timestart = Time.time;
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Source.volume = Curve.Evaluate(actualcurvevalue);
                    actualcurvevalue += curvedelta;
                    await AsyncUtility.WaitForSecondsOrCancel(durationstep - timedelay, ct);
                    timedelay = Time.time - timestart - durationstep + timedelay;
                }
                Source.volume = Curve.Evaluate(1);
            }
            catch
            {
                if (cancelaction == CancelAction.Final) Source.volume = Curve.Evaluate(1);
                else if (cancelaction == CancelAction.Initial) Source.volume = Curve.Evaluate(0);
            }
        }

        public static async Task CurveFadeInRealTimeAsync(AudioSource Source, AnimationCurve Curve, float duration, int iteration, CancelAction cancelaction, CancellationToken ct)
        {
            float curvedelta = 1f / iteration;
            float durationstep = duration / iteration;
            float timestart; //this variables are used to avoid time offset
            float timedelay = 0; //
            float actualcurvevalue = 0f;
            try
            {
                for (int i = 0; i < iteration; i++)
                {
                    timestart = Time.unscaledTime;
                    if (ct.IsCancellationRequested) throw new OperationCanceledException();
                    Source.volume = Curve.Evaluate(actualcurvevalue);
                    actualcurvevalue += curvedelta;
                    await AsyncUtility.WaitForSecondsRealtimeOrCancel(durationstep - timedelay, ct);
                    timedelay = Time.unscaledTime - timestart - durationstep + timedelay;
                }
                Source.volume = Curve.Evaluate(1);
            }
            catch
            {
                if (cancelaction == CancelAction.Final) Source.volume = Curve.Evaluate(1);
                else if (cancelaction == CancelAction.Initial) Source.volume = Curve.Evaluate(0);
            }
        }
    }

}