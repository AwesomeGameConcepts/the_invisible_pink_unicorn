using UnityEditor;
using UnityEngine;

public enum ReadOnlyEnum
{
    Always,
    OnEditor,
    OnPlay
}

public class ReadOnlyAttribute : PropertyAttribute
{
    public ReadOnlyEnum When;
}
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        ReadOnlyAttribute readonlyAttribute = attribute as ReadOnlyAttribute;

        if (readonlyAttribute.When == ReadOnlyEnum.Always ||
            (Application.isPlaying && readonlyAttribute.When == ReadOnlyEnum.OnPlay) ||
            (!Application.isPlaying && readonlyAttribute.When == ReadOnlyEnum.OnEditor))
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
        else
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
    }
}
#endif