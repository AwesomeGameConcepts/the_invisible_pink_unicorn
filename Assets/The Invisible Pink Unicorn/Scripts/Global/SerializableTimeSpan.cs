﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class SerializableTimeSpan : ISerializationCallbackReceiver
{
    public TimeSpan timeSpan;

    [SerializeField]
    private string _timeSpan = default;

    public static implicit operator TimeSpan(SerializableTimeSpan sts)
    {
        return (sts.timeSpan);
    }

    public static implicit operator SerializableTimeSpan(TimeSpan ts)
    {
        return new SerializableTimeSpan() { timeSpan = ts };
    }
    public static SerializableTimeSpan operator +(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan + ts;
    }
    public static SerializableTimeSpan operator -(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan - ts;
    }
    public static bool operator <(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan < ts;
    }
    public static bool operator >(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan > ts;
    }
    public static bool operator <=(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan <= ts;
    }
    public static bool operator >=(SerializableTimeSpan sts, TimeSpan ts)
    {
        return sts.timeSpan >= ts;
    }
    public static bool operator <=(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan <= TimeSpan.FromSeconds(i);
    }
    public static bool operator >=(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan >= TimeSpan.FromSeconds(i);
    }
    public static bool operator <(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan < TimeSpan.FromSeconds(i);
    }
    public static bool operator >(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan > TimeSpan.FromSeconds(i);
    }
    public static bool operator ==(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan.Seconds == i;
    }
    public static bool operator !=(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan.Seconds != i;
    }
    public static SerializableTimeSpan operator -(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan - TimeSpan.FromSeconds(i);
    }
    public static SerializableTimeSpan operator +(SerializableTimeSpan sts, int i)
    {
        return sts.timeSpan + TimeSpan.FromSeconds(i);
    }
    public static SerializableTimeSpan operator /(SerializableTimeSpan sts, int i)
    {
        return new TimeSpan(sts.timeSpan.Ticks / i);
    }
    public static SerializableTimeSpan operator +(SerializableTimeSpan sts, float f)
    {
        return sts.timeSpan + TimeSpan.FromSeconds(f);
    }
    public void OnAfterDeserialize()
    {
        TimeSpan.TryParse(_timeSpan, out timeSpan);
    }

    public void OnBeforeSerialize()
    {
        _timeSpan = timeSpan.ToString();
    }

    public override string ToString() => timeSpan.ToString();

    public override bool Equals(object obj) => obj is SerializableTimeSpan span && timeSpan.Equals(span.timeSpan) && _timeSpan == span._timeSpan;

    public override int GetHashCode()
    {
        var hashCode = -1166360289;
        hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(timeSpan);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_timeSpan);
        return hashCode;
    }
}