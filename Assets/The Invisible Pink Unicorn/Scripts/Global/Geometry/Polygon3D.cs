﻿using UnityEngine;
using System.Collections;

public class Polygon3D : MonoBehaviour
{
    [SerializeField] MeshGenerator m_MeshGenerator = default;
    [SerializeField] Mesh m_Mesh = default;
    // Use this for initialization

    void Start()
    {
        //Destroy(m_MeshGenerator.gameObject);
        //Destroy(this);
    }
    

    public void GenerateMesh()
    {
        //m_MeshGenerator.Construct();
        m_Mesh = m_MeshGenerator.Mesh;
        m_Mesh.name = "Polygon3D";
        GetComponent<MeshFilter>().mesh = m_Mesh;
        GetComponent<MeshCollider>().sharedMesh = m_Mesh;
    }

    public Mesh Mesh => m_Mesh;
}
