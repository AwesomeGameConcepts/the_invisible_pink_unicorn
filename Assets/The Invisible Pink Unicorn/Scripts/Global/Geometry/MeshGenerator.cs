﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class MeshGenerator : MonoBehaviour
{
   
    
    [SerializeField] private int m_NbBaseVertices = default;
    [SerializeField] private string m_MeshName = "Polygon3D";
    [SerializeField] private Transform m_Base = default;
    [SerializeField] private Transform m_Top = default;
    [SerializeField] private GameObject m_Model = default;
    [SerializeField] private Color m_VertexColor = default;
    [SerializeField] private Color m_TrianglesColor = default;

    private float m_Height;
    private List<Transform> m_Triangles = new List<Transform>();

    public bool m_DrawVertices;
    public bool m_DrawTriangles;
    
    //public Vector3[] vert;
    //public Vector3[] normal;

    public Mesh Mesh{ get {
            Mesh mesh = new Mesh();
            //vert = Vertices;
            mesh.vertices = Vertices;
            mesh.triangles = Triangles;

            /*normal = new Vector3[vert.Length];
            for (int i = 0; i < vert.Length; i++)
            {
                normal[i] = new Vector3(0,0.1f,0);
            }
            mesh.normals = normal;*/

            mesh.name = m_MeshName;
            return mesh;
        }}

    public void AddVertices( int num)
    {
        GameObject go;
        GameObject[] selectedgo = new GameObject[2*num];
        for(int i=0; i< num; i++)
        {
            go = Instantiate<GameObject>(m_Model, m_Base);
            go.name = "B" + m_Base.childCount;
            go.SetActive(true);
            if (m_Base.childCount > 1) go.transform.localPosition = m_Base.GetChild(m_Base.childCount - 2).localPosition;
            else go.transform.localPosition = Vector3.zero;
            selectedgo[i * 2] = go;
            go = Instantiate<GameObject>(m_Model, m_Top);
            go.name = "T" + m_Top.childCount;
            go.SetActive(true);
            go.transform.localPosition = m_Base.GetChild(m_Base.childCount - 1).localPosition;
            selectedgo[i * 2+1] = go;
        }
        m_NbBaseVertices += num;
        CalculateTriangles();
#if UNITY_EDITOR
        Selection.objects = selectedgo;
#endif
    }

    public void RemoveVertice(int index)
    {
        DestroyImmediate(m_Base.GetChild(index).gameObject);
        DestroyImmediate(m_Top.GetChild(index).gameObject);
        m_NbBaseVertices--;
        CalculateTriangles();
    }


    /// <summary>
    /// draw all the vertex and triangles
    /// </summary>
    void OnDrawGizmos()
    {
        if (m_DrawVertices)
        {
            Gizmos.color = m_VertexColor;
            for (int i = 0; i < m_NbBaseVertices - 1; i++)
            {
                //draw base vertex
                Gizmos.DrawLine(m_Base.GetChild(i).position, m_Base.GetChild(i + 1).position);
                //draw top vertex
                Gizmos.DrawLine(m_Top.GetChild(i).position, m_Top.GetChild(i + 1).position);
                //draw height vertex
                Gizmos.DrawLine(m_Base.GetChild(i).position, m_Top.GetChild(i).position);
            }
            //draw closing vertex
            Gizmos.DrawLine(m_Base.GetChild(m_Base.childCount - 1).position, m_Base.GetChild(0).position);
            Gizmos.DrawLine(m_Top.GetChild(m_Top.childCount - 1).position, m_Top.GetChild(0).position);
            Gizmos.DrawLine(m_Base.GetChild(m_Base.childCount - 1).position, m_Top.GetChild(m_Base.childCount - 1).position);
        }
        if (m_DrawTriangles)
        {
            // draw triangles
            for (int i = 0; i < m_Triangles.Count / 3; i++) GizmoExtensions.DrawTriangle(m_Triangles[i * 3].position, m_Triangles[i * 3 + 1].position, m_Triangles[i * 3 + 2].position, m_TrianglesColor);
        }
    }

    public void CalculateTriangles()
    {
        m_Triangles.Clear();
        if(m_NbBaseVertices >1)
        {
            //height triangles
            int b=0;
            int t=0;
            for (int i = 0; i < m_NbBaseVertices*2; i++)
            {
                if(i%2 == 0)
                {
                    m_Triangles.Add(m_Base.GetChild(b));
                    b++;
                    if (b == m_NbBaseVertices) b = 0;
                    m_Triangles.Add(m_Base.GetChild(b));
                    m_Triangles.Add(m_Top.GetChild(t));
                }
                else
                {
                    m_Triangles.Add(m_Base.GetChild(b));
                    m_Triangles.Add(m_Top.GetChild(t));
                    t++;
                    if (t == m_NbBaseVertices) t = 0;
                    m_Triangles.Add(m_Top.GetChild(t));
                }
            }
            //top and base triangles
            if(m_NbBaseVertices > 2 )
            {
                m_Triangles.Add(m_Base.GetChild(0));
                m_Triangles.Add(m_Base.GetChild(1));
                m_Triangles.Add(m_Base.GetChild(2));
                m_Triangles.Add(m_Top.GetChild(0));
                m_Triangles.Add(m_Top.GetChild(1));
                m_Triangles.Add(m_Top.GetChild(2));
                for (int i = 0; i < m_NbBaseVertices-2; i++)
                {
                    m_Triangles.Add(m_Base.GetChild(0));
                    m_Triangles.Add(m_Base.GetChild(i + 1));
                    m_Triangles.Add(m_Base.GetChild(i + 2));
                    m_Triangles.Add(m_Top.GetChild(0));
                    m_Triangles.Add(m_Top.GetChild(i + 1));
                    m_Triangles.Add(m_Top.GetChild(i + 2));
                }
            }
        }
    }

    public int NbBaseVertices { get => m_NbBaseVertices; set
        {
            if(value != m_NbBaseVertices)
            {
                if(value > m_NbBaseVertices) AddVertices(value - m_NbBaseVertices);
                else
                {
                    float nbtoremove = m_NbBaseVertices - value;
                    for(int i =0; i < nbtoremove; i++)
                    {
                        RemoveVertice(m_NbBaseVertices - i - 1);
                    }
                }
            }
        } }

    private Vector3[] Vertices {  get
        {
            Vector3[] vertices = new Vector3[m_Base.childCount + m_Top.childCount];
            for (int i = 0; i < m_Base.childCount; i++)
            {
                vertices[i] = (m_Base.GetChild(i).localPosition+ m_Base.localPosition).Multiply(m_Base.localScale);
            }
            for (int i = 0; i < m_Top.childCount; i++)
            {
                vertices[i + m_Base.childCount] = (m_Top.GetChild(i).localPosition +m_Top.localPosition).Multiply(m_Top.localScale); ;
            }
            return vertices;
        } }

    private int[] Triangles { get
        {
            int[] triangles = new int[m_Triangles.Count];
            int b ;
            int t;
            bool find;
            for (int i = 0; i < m_Triangles.Count; i++)
            {
                b = 0;
                t = 0;
                find = false;
                for (b = 0; b < m_Base.childCount; b++)
                {
                    if (m_Base.GetChild(b) == m_Triangles[i])
                    {
                        triangles[i] = b;
                        find = true;
                    }
                }
                if(!find)
                {
                    for (t = 0; t < m_Top.childCount; t++)
                    {
                        if (m_Top.GetChild(t) == m_Triangles[i]) triangles[i] = b + t;
                    }
                }
            }
            return triangles;
        } }

    public float Height { get => m_Height; set
        {
            m_Top.localPosition = new Vector3(m_Top.localPosition.x, m_Height, m_Top.localPosition.z);
            m_Height = value;

        } }
    
    /*
    public void Construct()
    {
        Mesh = new Mesh();
        //m_BaseVertices = new Vector3[1];
        m_CalculatedVertices = m_BaseVertices.AddArray<Vector3>(TopVertices);
        Mesh.vertices = m_CalculatedVertices;
        //mesh.uv = newUV;
        m_CalculatedTriangles = m_BaseTriangles2.AddArray<int>(TopTriangles).AddArray<int>(HeightTriangles);
        Mesh.triangles = m_CalculatedTriangles;


    }

    private Vector3[] TopVertices { get
        {
            Vector3[] Top = new Vector3[m_BaseVertices.Length];
            for(int i =0; i < m_BaseVertices.Length; i++)
            {
                Top[i] = m_BaseVertices[i] + Vector3.up * m_Height;
            }
            return Top;
        } }

    private int[] TopTriangles
    {
        get
        {
            int[] Top = new int[m_BaseTriangles2.Length];
            for (int i = 0; i < m_BaseTriangles2.Length; i++)
            {
                // a modifier
                Top[i] = m_BaseVertices.Length + m_BaseTriangles2[i];
            }
            return Top;
        }
    }

    private int[] HeightTriangles { get
        {
            int[] Triangles = new int[m_BaseVertices.Length* 6];
            int b = 0;
            int t = m_BaseVertices.Length;
            for (int i = 0; i < Triangles.Length/3; i++)
            {
                if(i % 2 ==0)
                {
                    Triangles[i * 3] = b;
                    b += 1;
                    Triangles[i * 3+1] = b;
                    Triangles[i * 3+2] = t;
                }
                else
                {
                    Triangles[i * 3] = b;
                    Triangles[i * 3 + 1] = t;
                    t += 1;
                    Triangles[i * 3 + 2] = t;
                }
                Debug.Log("Bulding tringle n°" + i + " points " + Triangles[i * 3] + " " + Triangles[i * 3 + 1] + " " + Triangles[i * 3 + 2]);
            }
            Triangles[Triangles.Length - 1] = 0;
            return Triangles;
        } }
    */
}
