﻿using AGC;
using System;
using UnityEngine;
using UnityEngine.Events;



[System.Serializable]
public struct NamedAudio
{
    public string Name;
    public AudioClip Clip;
}

[System.Serializable]
public struct NamedEvent
{
    public string Name;
    public UnityEvent Event;
    public bool Active;
}
