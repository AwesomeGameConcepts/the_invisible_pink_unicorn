﻿using UnityEngine;

/// <summary>
/// Mother class for all area with trigger collider
/// </summary>
public class Area : MonoBehaviour
{
    public enum ColliderType { Sphere, Box };
    [Tooltip("Type of collider will be created")] [SerializeField] protected ColliderType m_ColliderType = default;
    [Tooltip("Position of the Collider")] [SerializeField] protected Vector3 m_Position = default;
    [Tooltip("Center of the Collider")] [SerializeField] protected Vector3 m_Center = default;
    [Tooltip("Size of the box, use only if ColliderType == Box")] [SerializeField] protected Vector3 m_BoxSize = new Vector3(1, 1, 1);
    [Tooltip("Radius of the sphere, use only if ColliderType == Sphere")] [SerializeField] protected float m_SpereRadius = 1f;

    public Vector3 Position { get { return m_Position; } }
    public Vector3 Center { get { return m_Center; } }
    public Vector3 BoxSize { get { return m_BoxSize; } }
    public float SphereRadius { get { return m_SpereRadius; } }

    /// <summary>
    /// Used in Editor for adding area with trigger collider
    /// </summary>
    /// <param name="position">the position in the world</param>
    /// <param name="center">the center of the collider</param>
    /// <param name="boxsize">the size of the collider box, used only if the collider type in the ispector is set to box</param>
    /// <param name="sphereradius">the radius of the collider sphere, used only if the collider type in the ispector is set to sphere </param>
    /// <param name="components">the types of components to add</param>
    /// <returns></returns>
    public virtual GameObject AddArea(Vector3 position, Vector3 center, Vector3 boxsize, float sphereradius, System.Type[] components)
    {
        GameObject Instance = new GameObject("Area" + (transform.childCount + 1), components);
        Instance.transform.SetParent(transform);
        if (m_ColliderType == ColliderType.Sphere)
        {
            Instance.AddComponent<SphereCollider>();
            Instance.GetComponent<SphereCollider>().center = center;
            Instance.GetComponent<SphereCollider>().radius = sphereradius;
        }
        else
        {
            Instance.AddComponent<BoxCollider>();
            Instance.GetComponent<BoxCollider>().center = center;
            Instance.GetComponent<BoxCollider>().size = boxsize;
        }
        Instance.transform.localPosition = position;
        Instance.GetComponent<Collider>().isTrigger = true;
        return Instance;
    }
}
