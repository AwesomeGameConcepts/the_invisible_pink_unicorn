﻿using UnityEngine;
using System.Collections;
namespace AGC
{
    public enum Direction2D { Left, Right, Up, Down };
    public enum CancelAction { Stop, Initial, Final };

    public enum ModelSource
    {
        Imported = 0x0,
        Asset = 0x1,
        Scene = 0x2,
        AdditionalVertexStreams = 0x3,
        Error = 0x4
    }
}