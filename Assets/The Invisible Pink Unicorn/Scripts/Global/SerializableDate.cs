﻿using UnityEngine;
using System;

[Serializable]
public class SerializableDate : ISerializationCallbackReceiver
{
    public DateTime dateTime;

    [SerializeField]
    private string _dateTime = default;

    public static implicit operator DateTime(SerializableDate sd)
    {
        return (sd.dateTime);
    }

    public static implicit operator SerializableDate(DateTime dt)
    {
        return new SerializableDate() { dateTime = dt };
    }
    public static SerializableDate operator +(SerializableDate d, TimeSpan t)
    {
        return d.dateTime + t;
    }
    public static TimeSpan operator -(SerializableDate d1, SerializableDate d2)
    {
        return d1.dateTime - d2.dateTime;
    }

    public void OnAfterDeserialize()
    {
        DateTime.TryParse(_dateTime, out dateTime);
    }

    public void OnBeforeSerialize()
    {
        _dateTime = dateTime.ToString();
    }

    public override string ToString()
    {
        return dateTime.ToString();
    }
    
}