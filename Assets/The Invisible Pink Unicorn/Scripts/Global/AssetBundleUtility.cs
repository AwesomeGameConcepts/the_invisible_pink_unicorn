﻿using UnityEngine;
using System.Threading.Tasks;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace AGC
{
    public static class AssetBundleUtility
    {
        public enum AssetLocation { Disk, Memory, Server};
        public static string StreamingAssetBundlePath = "AssetBundles";
        public static string AssetPath = Path.Combine(Application.streamingAssetsPath, StreamingAssetBundlePath, PlatformUtility.OS);
        public static string ManifestBundlePath = Path.Combine(AssetPath, PlatformUtility.OS);
        /*
        #region Web

        public static async Task<AssetBundle> DownloadAndCacheAssetBundleAsync(string bundleName)
        {
            //Load the manifest
            AssetBundle manifestBundle = AssetBundle.LoadFromFile(ManifestBundlePath);
            AssetBundleManifest manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");

            /Create new cache
            Cache newCache = Caching.AddCache(CacheUtility.m_CachePath);

            //Set current cache for writing to the new cache if the cache is valid
            if (newCache.valid)
                Caching.currentCacheForWriting = newCache;
            Debug.Log(Caching.cacheCount);
            //Download the bundle
            Hash128 hash = manifest.GetAssetBundleHash(bundleName);
            UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(Path.Combine(ServerUtility.AssetPath, bundleName), hash, 0);
            await request.SendWebRequest();
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);

            if (bundle != null)
            {
                //Get all the cached versions
                List<Hash128> listOfCachedVersions = new List<Hash128>();
                Caching.GetCachedVersions(bundle.name, listOfCachedVersions);

                if (!AssetBundleContainsAssetIWantToLoad(bundle)) //Or any conditions you want to check on your new asset bundle
                {
                    //If our criteria wasn't met, we can remove the new cache and revert back to the most recent one
                    //Caching.currentCacheForWriting = Caching.GetCacheAt(Caching.cacheCount);
                    //Caching.RemoveCache(newCache);

                    for (int i = listOfCachedVersions.Count - 1; i > 0; i--)
                    {
                        //Load a different bundle from a different cache
                        request = UnityWebRequestAssetBundle.GetAssetBundle(Path.Combine(ServerUtility.AssetPath, bundleName), listOfCachedVersions[i], 0);
                        await request.SendWebRequest();
                        bundle = DownloadHandlerAssetBundle.GetContent(request);
                        //Check and see if the newly loaded bundle from the cache meets your criteria
                        if (AssetBundleContainsAssetIWantToLoad(bundle))
                            break;
                    }
                }
                else
                {
                    //This is if we only want to keep 5 local caches at any time
                    if (Caching.cacheCount > 5)
                        Caching.RemoveCache(Caching.GetCacheAt(1));     //Removes the oldest user created cache
                }
            }
            else Debug.LogError("Can't find on server Assetbundle : " + bundleName);
            return bundle;
        }

        private static bool AssetBundleContainsAssetIWantToLoad(AssetBundle bundle)
        {
            return (bundle.LoadAsset<GameObject>("Cube") != null);     //this could be any conditional
        }
        #endregion
*/
        #region Manifest
        public static async Task<uint> AssetBundleCRC(string assetBundleName, AssetLocation location)
        {
            UnityWebRequest manifest = null;
            switch (location)
            {
                case AssetLocation.Disk:
                    manifest = UnityWebRequest.Get(Path.Combine(AssetPath, assetBundleName) + ".manifest");
                    break;
                case AssetLocation.Memory:
                    throw new NotImplementedException();
                    manifest = UnityWebRequest.Get(Path.Combine(AssetPath, assetBundleName) + ".manifest");
                    break;
                case AssetLocation.Server:
                    //manifest = UnityWebRequest.Get(Path.Combine(ServerUtility.AssetPath, assetBundleName) + ".manifest");
                    break;
            }
            await manifest.SendWebRequest();
            if (!string.IsNullOrEmpty(manifest.error))
            {
                Debug.LogError("Failed to get manifest file for " + assetBundleName + "  from URL '" + manifest.url + "' :\n\t" + manifest.error);
                return 0;
            }
            else
            {
                string[] lines = manifest.downloadHandler.text.Split('\n');
                if (lines[1].Contains("CRC:")) return uint.Parse(lines[1].Substring(5, lines[1].Length - 5));
                else return 0;
            }
        }
        #endregion

        #region AssetBundle
        public static AssetBundleCreateRequest CreateAssetBundleRequest(string assetBundleName, AssetLocation location)
        {
            string filePath = Path.Combine(AssetPath, assetBundleName.ToLower());
            AssetBundleCreateRequest LoadRequest = null;
            switch (location)
            {
                case AssetLocation.Disk:
                    LoadRequest = AssetBundle.LoadFromFileAsync(filePath);
                    break;
                    /*
                case AssetLocation.Memory:
                    throw new NotImplementedException();
                    assetBundle = await AssetBundle.LoadFromMemoryAsync(File.ReadAllBytes(filePath));
                    break;
                case AssetLocation.Server:
                    assetBundle = await DownloadAndCacheAssetBundleAsync(assetBundleName);
                    break;*/
            }
            if (LoadRequest != null) return LoadRequest;
            else Debug.Log("no AssetBundle " + assetBundleName + " found.");
            return null;
        }
        public static async Task<AssetBundle> LoadAssetBundleAsync(string assetBundleName, AssetLocation location)
        {
            string filePath = Path.Combine(AssetPath, assetBundleName.ToLower());
            AssetBundle assetBundle = null;
            switch (location)
            {
                case AssetLocation.Disk:
                    assetBundle = await AssetBundle.LoadFromFileAsync(filePath);
                    break;
                case AssetLocation.Memory:
                    throw new NotImplementedException();
                    assetBundle = await AssetBundle.LoadFromMemoryAsync(File.ReadAllBytes(filePath));
                    break;
                case AssetLocation.Server:
                    //assetBundle = await DownloadAndCacheAssetBundleAsync(assetBundleName);
                    break;
            }
            if (assetBundle != null) return assetBundle;
            else Debug.Log("no AssetBundle " + assetBundleName + " found.");
            return null;
        }

        public static AssetBundle LoadAssetBundle(string assetBundleName, AssetLocation location)
        {
            string filePath = Path.Combine(AssetPath, assetBundleName.ToLower());
            AssetBundle assetBundle = null;
            switch (location)
            {
                case AssetLocation.Disk:
                    assetBundle = AssetBundle.LoadFromFile(filePath);
                    break;
                case AssetLocation.Memory:
                    throw new NotImplementedException();
                    assetBundle = AssetBundle.LoadFromMemory(File.ReadAllBytes(filePath));
                    break;
                case AssetLocation.Server:
                    Debug.LogError("Can't load AssetBundle from server without asyn operation");
                    assetBundle = null;
                    break;
            }
            if (assetBundle != null) return assetBundle;
            else Debug.Log("no AssetBundle " + assetBundleName + " found.");
            return null;
        }
#endregion

#region Asset
        public static T LoadAsset<T>(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            AssetBundle assetBundle = LoadAssetBundle(assetBundleName, location);
            if (assetBundle != null)
            {
                if (assetBundle.Contains(objectNameToLoad))
                {
                    T loadedobject = assetBundle.LoadAsset<T>(objectNameToLoad);
                    assetBundle.Unload(unload);
                    return loadedobject;
                }
                else Debug.LogError("Can't find asset " + objectNameToLoad + " in " + assetBundleName + " assetbundle.");
            }
            return null;
        }
        
        public static async Task<T> LoadAssetAsync<T>(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            AssetBundle assetBundle = await LoadAssetBundleAsync(assetBundleName, location);
            if (assetBundle != null)
            {
                if (assetBundle.Contains(objectNameToLoad))
                {
                    T Obj = await assetBundle.LoadAssetAsync<T>(objectNameToLoad) as T;
                    assetBundle.Unload(unload);
                    return Obj;
                }
                else Debug.LogError("Can't find bundle " + objectNameToLoad + " in " + assetBundleName + " assetbundle.");
            }
            return null;
        }

        public static async Task<T[]> LoadAssetAsync<T>(string assetBundleName, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            AssetBundle assetBundle = await LoadAssetBundleAsync(assetBundleName, location);
            if (assetBundle != null)
            {
                T[] Obj = new T[assetBundle.GetAllAssetNames().Length];
                for (int i = 0; i < Obj.Length; i++)
                {
                    Obj[i] = await assetBundle.LoadAssetAsync<T>(assetBundle.GetAllAssetNames()[i]) as T;
                }
                assetBundle.Unload(unload);
                return Obj;
            }
            return null;
        }
        
        public static async Task<T> LoadAssetFromWebAsync<T>(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle("http://www.awesomegameconcepts.com/Assets/test");
            await uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
                return null;
            }
            else
            {
                // Get downloaded asset bundle
                AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(uwr);
                T obj = await assetBundle.LoadAssetAsync<T>(objectNameToLoad) as T; 
                assetBundle.Unload(unload);
                return obj;
            }
        }

#endregion

#region Text
        public static string LoadText(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload)
        {
            AssetBundle assetBundle = LoadAssetBundle(assetBundleName, location);
            if (assetBundle != null)
            {
                if (assetBundle.Contains(objectNameToLoad))
                {
                    TextAsset loadedTextAsset = assetBundle.LoadAsset<TextAsset>(objectNameToLoad);
                    assetBundle.Unload(unload);
                    return loadedTextAsset.text;
                }
                else Debug.LogError("Can't find bundle " + objectNameToLoad + " in " + assetBundleName + " assetbundle.");
            }
            return null;
        }

        public static async Task<string> LoadTextAsync(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload)
        {
            AssetBundle assetBundle = await LoadAssetBundleAsync(assetBundleName, location);
            if (assetBundle != null)
            {
                if (assetBundle.Contains(objectNameToLoad))
                {
                    TextAsset textAsset = await assetBundle.LoadAssetAsync<TextAsset>(objectNameToLoad) as TextAsset;
                    assetBundle.Unload(unload);
                    return textAsset.text;
                }
                else Debug.LogError("Can't find bundle " + objectNameToLoad + " in " + assetBundleName + " assetbundle.");
            }
            return null;
        }

        public static async Task<string[]> LoadTextAsync(string assetBundleName, AssetLocation location, bool unload)
        {
            AssetBundle assetBundle = await LoadAssetBundleAsync(assetBundleName, location);
            if (assetBundle != null)
            {
                string[] Texts = new string[assetBundle.GetAllAssetNames().Length];
                for (int i = 0; i < Texts.Length; i++)
                {
                    Texts[i] = (await assetBundle.LoadAssetAsync<TextAsset>(assetBundle.GetAllAssetNames()[i]) as TextAsset).text;
                }
                assetBundle.Unload(unload);
                return Texts;
            }
            return null;
        }

#endregion

        /// <summary>
        /// instantiate async an object of type T where name equal to objectNameToLoad
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetBundleName"></param>
        /// <param name="objectNameToLoad"></param>
        /// <param name="location"></param>
        /// <param name="unload"></param>
        /// <returns></returns>
        public static async Task<T> InstantiateFromAssetBundleAsync<T>(string assetBundleName, string objectNameToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            return UnityEngine.Object.Instantiate(await LoadAssetAsync<T>(assetBundleName, objectNameToLoad, location, unload));
        }

        /// <summary>
        /// instatiate async all objects of type T if their name are contained in objectsNamesToLoad
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetBundleName"></param>
        /// <param name="objectsNamesToLoad"></param>
        /// <param name="location"></param>
        /// <param name="unload"></param>
        /// <returns></returns>
        public static async Task<T[]> InstantiateFromAssetBundleAsync<T>(string assetBundleName, string[] objectsNamesToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            T[] result = new T[objectsNamesToLoad.Length];
            T[] tmp = await LoadAssetAsync<T>(assetBundleName, location, unload);
            for (int j = 0; j < objectsNamesToLoad.Length; j++)
            {
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (objectsNamesToLoad[j] == tmp[i].name)
                    {
                        result[j] = UnityEngine.Object.Instantiate(tmp[i]);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// instatiate async all objects of type T if their name are contained in objectsNamesToLoad in Scene scene
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="scene"></param>
        /// <param name="assetBundleName"></param>
        /// <param name="objectsNamesToLoad"></param>
        /// <param name="location"></param>
        /// <param name="unload"></param>
        /// <returns></returns>
        public static async Task<T[]> InstantiateFromAssetBundleAsync<T>(Scene scene, string assetBundleName, string[] objectsNamesToLoad, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            T[] result = new T[objectsNamesToLoad.Length];
            T[] tmp = await LoadAssetAsync<T>(assetBundleName, location, unload);
            for (int j = 0; j < objectsNamesToLoad.Length; j++)
            {
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (objectsNamesToLoad[j] == tmp[i].name)
                    {
                        result[j] = UnityEngine.Object.Instantiate(tmp[i]);
                        SceneManager.MoveGameObjectToScene(result[j] as GameObject, scene);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Instatiate async all objects of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetBundleName"></param>
        /// <param name="location"></param>
        /// <param name="unload"></param>
        /// <returns></returns>
        public static async Task<T[]> InstantiateFromAssetBundleAsync<T>(string assetBundleName, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            T[] result = await LoadAssetAsync<T>(assetBundleName, location, unload);
            foreach (T type in result) UnityEngine.Object.Instantiate(type);
            return result;
        }
        /// <summary>
        /// Instatiate async all objects of type T in 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetBundleName"></param>
        /// <param name="location"></param>
        /// <param name="unload"></param>
        /// <returns></returns>
        public static async Task<T[]> InstantiateFromAssetBundleAsync<T>(Scene scene, string assetBundleName, AssetLocation location, bool unload) where T : UnityEngine.Object
        {
            T[] result = await LoadAssetAsync<T>(assetBundleName, location, unload);
            foreach (T type in result)
            {
                SceneManager.MoveGameObjectToScene(UnityEngine.Object.Instantiate(type) as GameObject, scene);
            }
            return result;
        }

    }
}