﻿using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
    [CreateAssetMenu(menuName = "Data/Boolean")]
    public class BooleanData : VariableData<bool>
    {
        public static implicit operator bool(BooleanData d) => d.Value;
    }
}