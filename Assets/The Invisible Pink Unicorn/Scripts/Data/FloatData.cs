﻿using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
    [CreateAssetMenu(menuName = "Data/Float")]
    public class FloatData : VariableData<float>
    {
        public static implicit operator float(FloatData d) => d.Value;

        [SerializeField]
        public AnimationCurve TweenCurve;

        public async Task Tween(float from, float to, float durationMs)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < durationMs)
            {
                float percent = sw.ElapsedMilliseconds / durationMs;
                if (TweenCurve != null)
                    percent = TweenCurve.Evaluate(percent);

                Value = Mathf.Lerp(from, to, percent);

                await Task.Yield();
            }

            Value = to;
        }

        public async Task Tween(float to, float durationMs) => await Tween(Value, to, durationMs);
    }
}