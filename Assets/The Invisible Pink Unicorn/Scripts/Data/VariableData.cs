﻿using System;
using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
    public abstract class VariableData<T> : ScriptableObject
    {
        [TextArea]
        [SerializeField]
        private string _description;

        [ReadOnly(When = ReadOnlyEnum.OnPlay)]
        [SerializeField]
        private T _initialValue;

        [SerializeField]
        private string _unit;

        [SerializeField]
        [ReadOnly(When = ReadOnlyEnum.OnEditor)]
        private T _workingValue;

        public event Action<T> OnValueChanged;

        public T Value
        {
            get => _workingValue;

            set
            {
                if (!Equals(_workingValue, value))
                {
                    _workingValue = value;

                    OnValueChanged?.Invoke(_workingValue);
                }
            }
        }

        private void OnEnable()
        {
            _workingValue = _initialValue;
        }
    }
}