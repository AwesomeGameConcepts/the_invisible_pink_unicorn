﻿using System;
using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
    [CreateAssetMenu(menuName = "Data/Int")]
    public class IntData : VariableData<int>
    {
        public static implicit operator int(IntData d) => d.Value;
    }
}