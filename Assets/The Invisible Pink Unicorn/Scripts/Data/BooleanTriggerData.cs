﻿using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
    [CreateAssetMenu(menuName = "Triggers/Boolean")]
    public class BooleanTriggerData : TriggerData<bool>
    {
    }
}