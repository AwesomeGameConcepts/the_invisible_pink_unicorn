﻿using System;
using UnityEngine;

namespace Assets.Ekioo.Framework.Data
{
public abstract class TriggerData<T> : ScriptableObject
{
    public event Action<T> OnTrigger;

    public void Invoke(T data)
    {
        OnTrigger?.Invoke(data);
    }
}
}